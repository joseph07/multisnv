//
//  GibbsAnalysis.cpp
//  
//
//  Created by Malvina Josephidou on 06/02/2013.
//
//

#include <iostream>
#include <vector>
#include <iomanip>
#include <string>
#include <cstring>
#include<fstream>
#include<istream>
#include<string>
#include<sstream>
#include <algorithm> // for copy
#include <iterator> // for ostream_iterator
#include <stdlib.h>
#include <map>
#include <string.h>
#include <utility>
#include <cstring>
#include <iterator>
#include<sstream>
#include <cmath>
#include <stdio.h> //to use fopen etc
#include <algorithm> // for copy
#include <iterator> // for ostream_iterator
#include <stdlib.h>
#include <cstdlib>
#include <ctime>
#include <numeric>
#include <functional>
#include <boost/math/distributions/hypergeometric.hpp>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h> // TODO: Remember to disable assertions in production by including -DNDEBUG flag in Makefile for release

#include "rungibbs.h"
#include "utils.h"
#include "GibbsSampler.h"
#include "Settings.h"
#include "MAPstate.h"
#include "SampleResults.h"
#include "ResultsFactory.h"
using namespace boost::math;
using namespace std;


GibbsSampler runGibbsSampler(const GibbsSettings &gibbs_settings,
		const ProgramSettings &program_settings, const MultisampleData * ms_data) {

	unsigned int index = findMostFrequentElement(ms_data->data.at(0).counts.total);
	int initState = index2intState(index);
	//int initState = ref2state(ms_data->pos.ref);
	GibbsSampler pG ( ms_data->data.size(), initState);
	boost::ptr_vector<ConditionalDistr> conditional_distr;
	ConditionalDistr::setRef(ms_data->pos.ref);
	/* Assumes first column of the pileup corresponds to reads from normal sample.*/
	for (int i = 0 ; i < ms_data->data.size() ; i++) {
		conditional_distr.push_back(ConditionalDistr::createConditionalDistr
				( i == 0  ? true : false));
		conditional_distr.at(i).init(ms_data->data.at(i), gibbs_settings);
	}

	/* Start MCMC */
	if( gibbs_settings.checkConv == false) {
		pG.drawSamples(gibbs_settings, conditional_distr);
		pG.computeCounts(gibbs_settings.burnin, gibbs_settings.thin);
	}

	else if (gibbs_settings.checkConv == true) {
		bool isConverged = false;
		while(isConverged == false && pG.executedGibbsIters <= gibbs_settings.maxIter) {
			pG.drawSamples(gibbs_settings, conditional_distr); //Draw samples
			pG.computeCounts(gibbs_settings.burnin, gibbs_settings.thin);
			isConverged = pG.convergenceCheck();
			pG.executedGibbsIters += gibbs_settings.gibbs_runs;
		}
	}
	return pG;
}

vector <SampleResults> get_mutated_tumour_states(const vector <SampleResults> &tumour_samples, const SampleResults &normal_sample) {
	vector <SampleResults> mutated_tumour_states;
	for(int i = 0 ; i < tumour_samples.size(); i++) {
		if(normal_sample != tumour_samples.at(i) ) {
			if(find(mutated_tumour_states.begin(), mutated_tumour_states.end(), tumour_samples.at(i)) == mutated_tumour_states.end()) {
				mutated_tumour_states.push_back(tumour_samples.at(i));
			}
		}
	}
	return mutated_tumour_states;
}

variant_type_t get_event_type(const vector <SampleResults> &unique_tumour_samples, const SampleResults &normal_sample ) {
	if(unique_tumour_samples.size() > 1)
		return UNEXPECTED;
	else if(unique_tumour_samples.size() == 0)
		return normal_sample.get_somatic_status();

	SampleResults mutated_tumour = unique_tumour_samples.at(0);

	/* If any sample is more than two mutations away from the normal flag as unexpected */
	if(evolutionary_distance(string2Binary(mutated_tumour.get_MAP_state().get_as_str()),
							 string2Binary(normal_sample.get_MAP_state().get_as_str())) > 1)
		return UNEXPECTED;
	else
		return mutated_tumour.get_somatic_status();

	return UNEXPECTED;
}

void GibbsSampler::computeCounts(const double &burnin, const int &thin) {
    for( int j = 0 ; j < numberOfParameters ; j++) {
    	fill(samplerHistory.at(j) -> countsBin1.begin(), samplerHistory.at(j) -> countsBin1.end(), 0);
    	fill(samplerHistory.at(j) -> countsBin2.begin(), samplerHistory.at(j) -> countsBin2.end(), 0);
    }

    int totalCycles = samplerHistory.at(0)-> samples.size();
    int burninNo = int(totalCycles * burnin);

    for (int zz = burninNo ; zz < totalCycles - thin ; zz += thin + 1) {
        string aggregateState = "|";
        string str_sample;

        for( int j = 0 ; j < numberOfParameters ; j++) {
               str_sample = intState2string(samplerHistory.at(j)->samples.at(zz));
               aggregateState += str_sample + string( "|");

            if( zz < burninNo + 0.5* (totalCycles - burninNo)) {
               samplerHistory.at(j)-> countsBin1.at(samplerHistory.at(j)->samples.at(zz))++;
            }

            else {
               samplerHistory.at(j)-> countsBin2.at(samplerHistory.at(j)->samples.at(zz))++;
            }
        }
         countsPerAggregateState[aggregateState]++;
    }
    return;
}

bool GibbsSampler::convergenceCheck() {
	double pValue = 0.0;

	for(int j = 0; j < numberOfParameters ; j++) {
		vector <int> reducedBin1;
		vector <int> reducedBin2;
		assert(samplerHistory.at(j)->countsBin1.size() == samplerHistory.at(j)->countsBin2.size());
		for( int k = 0; k < samplerHistory.at(j)->countsBin1.size(); k++) {
			if ( samplerHistory.at(j)->countsBin1.at(k) != 0 || samplerHistory.at(j)->countsBin2.at(k) != 0 ) {
				reducedBin1.push_back( samplerHistory.at(j)->countsBin1.at(k));
				reducedBin2.push_back( samplerHistory.at(j)->countsBin2.at(k));
			}
		}
		assert(reducedBin1.size() == reducedBin2.size());
		if(reducedBin1.size() < 2) {
			continue;
		}

		else if(reducedBin1.size() == 2) {
			pValue = fisher_test(reducedBin1.at(0), reducedBin1.at(1), reducedBin2.at(0), reducedBin2.at(1));
		}

		else if(reducedBin1.size() > 2 ) {
			return true; // no convergence check for this case
		}

		if(pValue < 0.05) {
			return false;
		}
	}
    return true;
}

string reformat_genotype(string genotype) {
	/* We assume all genotypes will have at least 2 copies of the allele, i.e. A
	 * is actually AA : TODO: LOH events should actually stay as A i.e. if Normal is AC and tumour is A
	 * genotypes should be 0/1 and 0 instead of 0/1 and 0/0 */
	string reformatted = genotype;
	if(genotype.size() == 1 ) {
		reformatted = genotype + genotype;
	}
	return add_delimiter_to_string(reformatted, "/");
}

/* SOS : Never pass this by reference!! It will change the ptr_results->MAP element!!!!) */
vector <string> find_unique_vector_elements(vector <string> vec) {
	/* Sort and remove return vector with removed duplicates */
	sort(vec.begin(), vec.end());
	vec.erase( unique( vec.begin(), vec.end() ), vec.end());
	return(vec);
}

