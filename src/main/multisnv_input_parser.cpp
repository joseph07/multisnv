/*
 * multisnv_input_parser.cpp
 *
 *  Created on: Sep 20, 2014
 *      Author: malvinajosephidou
 */

#include "multisnv_input_parser.h"
#include "write_snp_header.h"
#include <vector>

using namespace std;
namespace po = boost::program_options;

po::options_description get_multisnv_options_desc(FilterSettings& filter_settings,
		ProgramSettings& program_settings, GibbsSettings& gibbs_settings )
{
    boost::program_options::options_description desc("multiSNV parameters");
    desc.add_options()
    ("help,H", "Produce this help message")
   	("version,v", "Print multiSNV version")
	("seed,S", po::value<int>(& program_settings.seed)->default_value(time(NULL), ""),
     "Set seed for random number generator. Defaults to time(NULL)")
    ("number,N", po::value<int>(& program_settings.NumberOfSamples),
     "Number of same-patient samples to analyse (required)")
     ("fasta", po::value<string> (& program_settings.ref_fasta),
      "Reference fasta file")
     ("bam", po::value<vector<string> >(& program_settings.bam_files)->multitoken(),
      "List of bam files. Place normal bam file FIRST! ")
     ("fout,f", po::value<string> (& program_settings.filename_output)->default_value("output.vcf"),
      "Specify name of VCF output file ")
     ("bed", po::value<string> (& program_settings.bed_file)->default_value(""),
       "Run multiSNV on regions specified in Bed file")
     ("regions", po::value<vector<string> >(& program_settings.regions)->multitoken(),
       "List of regions to analyse. Each region should be in one of these formats: chr11:start-end OR chr11:start (will"
       " analyse from specified start to end of chromosome) OR chr11 (will analyse entire chromosome)")
	("mu,m", po::value<double>(& gibbs_settings.mutrate)->default_value(0.000001, "0.000001"),
      "Probability of mutation")
     ("minBase,Q", po::value<int>(& filter_settings.ReadQualThreshold)->default_value(20),
       "Minimum acceptable base quality")
//    ("minIters", po::value<int>(& gibbs_settings.gibbs_runs)->default_value(300),
//          "Minimum number of Gibbs iterations")
//    ("maxIters", po::value<int>(& gibbs_settings.maxIter)->default_value(3000),
//          "Maximum number of Gibbs iterations")
     ("minMapQual,q", po::value<int>(& filter_settings.MappingQualThreshold)->default_value(30),
        "Minimum acceptable mapping quality")
	("dmin,d", po::value<int>(& filter_settings.min_depth)->default_value(5),
     "Minimum required depth (in normal and as an average across all samples)")
    ("low_depth", po::value<int>(& filter_settings.flag_low_depth)->default_value(6),
      "Flag site as low depth if average depth in tumour samples or the depth in the normal is less than this value")
    ("dmax,D", po::value<int>(& filter_settings.max_depth)->default_value(300),
     "Maximum acceptable depth in normal sample")
    ("mva,V", po::value<int>(& filter_settings.minVarAlleles)->default_value(1),
      "Run multiSNV only on sites where a tumour sample has at least this many variants ")
    ("weak_evidence", po::value<double>(& filter_settings.weakEvidenceThreshold)->default_value(0.03, "0.03"),
      "Flag site as weak evidence if none of the samples that have mutated meets this variant allele frequency threshold ")
    ("normal_contamination", po::value<double>(& filter_settings.normalContaminationThreshold)->default_value(0.03, "0.03"),
       "Flag site for possible normal contamination if variant is observed in the normal at a frequency higher than this threshold")
    ("minVariantReadsForTriallelicSite", po::value<int>(& filter_settings.minVariantReadsForTriallelicSite)->default_value(2),
               "The normal sample is flagged as triallelic if more than this number of reads are observed across 3 or more alleles")
    ("flag-homopolymer", po::value<int>(& filter_settings.homopolLengthThreshold)->default_value(5),
       "Flag site if variant is detected close to a flanking homopolymer with at least N"
       " repetitive bases. To avoid excessive filtering"
       " only sites where the variant is present in at least 1% of the reads in the normal will be flagged")
    ("medianT", po::value<int>(& gibbs_settings.MedianTumourCoverage)->default_value(40),
     "Median depth of coverage in tumour samples")
    ("medianN", po::value<int>(& gibbs_settings.MedianNormalCoverage)->default_value(40),
     "Median depth of coverage in normal sample")
    ("Rmdups", po::value<bool>(& filter_settings.Rmdups)->default_value(true),
          "Remove duplicates")
    ("include-germline", po::value<bool>(& program_settings.which_events[GERMLINE])->default_value(0),
      "Include heterozygous germline events in VCF ")
    ("include-LOH", po::value<bool>(& program_settings.which_events[LOH])->default_value(1),
       "Include LOH events in VCF ")
    ("print", po::value<bool>(& program_settings.print)->default_value(false),
        "Print mutation events ")
    ("conv", po::value<bool>(& gibbs_settings.checkConv)->default_value(false),
     "Do convergence checks")
     ("features", po::value<string> (& program_settings.features_file)->default_value(""), //if not specified default is "" and we will do nothing
       "Features output file ")
    ;
    return desc;
}

void
finalise_settings(const po::variables_map vm, FilterSettings& filter_settings,
		ProgramSettings& program_settings, GibbsSettings& gibbs_settings)
{
		if(gibbs_settings.burnin >= 1) {
			cerr << "Burn-in is the fraction of the chain you want to discard, "
					"consider setting it in range 0-0.5! " << endl;
			exit(EXIT_FAILURE);
		}

		if(filter_settings.min_depth < 1) {
			cerr << " Minimum depth cannot be less than 1! " << endl;
			exit(EXIT_FAILURE);
		}

		if(filter_settings.max_depth < 1) {
			cerr << " Maximum depth cannot be less than 1! " << endl;
			exit(EXIT_FAILURE);
		}

		if(program_settings.bam_files.empty()) {
			cerr << "Need to specify a list of bam files " << endl;
			exit(EXIT_FAILURE);
		}

		if(program_settings.ref_fasta.empty()) {
			cerr << "Need to specify a fasta file " << endl;
			exit(EXIT_FAILURE);
		}

		if(program_settings.NumberOfSamples == 0) {
			cerr << "You must specify the number of samples you wish to analyse" << endl;
			exit(EXIT_FAILURE);
		}

		if(program_settings.NumberOfSamples == 1) {
			cerr << "At least one normal and tumour sample are required" << endl;
			exit(EXIT_FAILURE);
		}

		if(program_settings.bam_files.size() != program_settings.NumberOfSamples ) {
			cerr << "The number of bam files listed does not match the number of samples " << endl;
			exit(EXIT_FAILURE);
		}

	    if( remove(program_settings.filename_output.c_str() ) == 0 )
	        cout << "Overwriting already existing VCF file" << endl;

	    if( remove(program_settings.features_file.c_str() ) == 0 && program_settings.features_file != "" )
	        cout << "Overwriting already existing feature file" << endl;

	    /* As in manuscript */
	    //gibbs_settings.weight  = 10.0 * program_settings.NumberOfSamples;
	    //gibbs_settings.weight  = program_settings.NumberOfSamples;
	    gibbs_settings.weight  = 1;
	    gibbs_settings.alpha   = 0.2 * gibbs_settings.MedianTumourCoverage;
	    gibbs_settings.alphaN  = 5 * gibbs_settings.MedianNormalCoverage;

	    /* If convergence checks are to be performed the number of gibbs runs
	     * must be at least 600.
	     * A thinning parameter is introduced and set to 5
	     * to reduce the autocorrelation of the Markov chain.
	     * This means that for every gibbs sample we use to estimate a sample
	     *  allelic composition the 5 next samples are discarded.
	     */
	    if(gibbs_settings.checkConv) {
	    	gibbs_settings.gibbs_runs = max(gibbs_settings.gibbs_runs, 600);
	    }

	    gibbs_settings.maxIter = 5 * gibbs_settings.gibbs_runs;

	return;
}

