// ***************************************************************************
// bamtools_FastaE.h (c) 2010 Derek Barnett, Erik Garrison
// Marth Lab, Department of Biology, Boston College
// ---------------------------------------------------------------------------
// Last modified: 10 October 2011
// ---------------------------------------------------------------------------
// Provides FastaE reading/indexing functionality.
// ***************************************************************************

//#ifndef BAMTOOLS_FastaE_H
//#define BAMTOOLS_FastaE_H

#ifndef FastaE_H
#define FastaE_H

#include "utils/utils_global.h"
#include <string>
#include <vector>

namespace BamTools {

class UTILS_EXPORT FastaE {
//class FastaE {
  
    // ctor & dtor
    public:
        FastaE(void);
        ~FastaE(void);
        
    // file-handling methods
    public:
        bool Close(void);
        bool Open(const std::string& filename, const std::string& indexFilename = "");
	
    // sequence access methods
    public:
        bool GetBase(const int& refID, const int& position, char& base);
        bool GetSequence(const int& refId, const int& start, const int& stop, std::string& sequence);
        
    // index-handling methods
    public:
		std::vector<std::string> GetReferenceNames();
		std::vector<int> GetReferenceLengths();
        bool CreateIndex(const std::string& indexFilename);

    // internal implementation
    private:
        struct FastaEPrivate;
        FastaEPrivate* d;
};
  
} // BAMTOOLS_FastaE_H
  
#endif // BAMTOOLS_FastaE_H
