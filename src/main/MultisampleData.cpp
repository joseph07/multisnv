/*
 * MultisampleData.cpp
 *
 *  Created on: Oct 28, 2014
 *      Author: joseph07
 */

#include <cmath>
#include <stdexcept>
#include "api/BamAux.h"

#include "MultisampleData.h"
#include "Data.h"
#include "Read.h"
#include "ResultsFactory.h"
#include "GibbsSampler.h"

using namespace std;

const string MultisampleData::bases = "ACGT";

MultisampleData::MultisampleData(size_t num)
	: pos(Locus())
	, readsFailingQC(0)
	, total_depth(0)
	, data(num, Data())
	{}

// TODO Auto-generated destructor stub
MultisampleData::~MultisampleData() {
}

void MultisampleData::CreateMultiPileup(const PileupPosition& pileupData,
		const FilterSettings &filter_settings, map <string, int > bam_to_ind) {

	int ambiguous = 0;
	int insertionCount = 0;
	int deletionCount = 0;
	int minQual = 0;
	int tempMQ = 0;

	for (vector<PileupAlignment>::const_iterator pileupIter = pileupData.PileupAlignments.begin();
			pileupIter != pileupData.PileupAlignments.end(); ++pileupIter)
	{
		const PileupAlignment& pa = (*pileupIter);
		const BamAlignment& ba = pa.Alignment;

		int index = bam_to_ind.find(ba.Filename)->second; // find which sample alignment belongs to

		// remove duplicates and vendor failed reads and also reads not forming a proper pair
		if ((filter_settings.Rmdups && ba.IsDuplicate())|| ba.IsFailedQC() || !ba.IsProperPair()) {
			continue;
		}

		// remove the reads with mapping qual<threshold
		if (ba.MapQuality < filter_settings.MappingQualThreshold) {
			readsFailingQC++;
			continue;
		}

		// adjacent insertions and deletions
		for (vector<CigarOp>::const_iterator opIter =
				ba.CigarData.begin(); opIter != ba.CigarData.end(); opIter++) {
			if (opIter->Type == 'I') {
				insertionCount++;
			}
			else if (opIter->Type == 'D') {
				deletionCount++;
			}
		}

		if (pa.IsCurrentDeletion) {
			continue;
		}

		char base = toupper(ba.QueryBases.at(pa.PositionInAlignment));

		if (base == 'N') {
			ambiguous++;
			continue;
		}

		// base quality, phred quality is scaled by 33
		minQual = ba.Qualities.at(pa.PositionInAlignment) - 33;

		// skip bases with base quality less than readQualThreshold
		if(minQual < filter_settings.ReadQualThreshold) {
			readsFailingQC++;
			continue;
		}

		/*TODO: Changed this back: minQual is the base quality - mapping error will not highly correlated anyway and
		 * artefacts arising from incorrect mapping should be picked up by downstream filters
		 *
		 * minQual = ba.MapQuality <= minQual ? ba.MapQuality : minQual ; */

		int baseIdx;
		switch (base) {
			case 'A': baseIdx = 0; break;
			case 'C': baseIdx = 1; break;
			case 'G': baseIdx = 2; break;
			case 'T': baseIdx = 3; break;
			default: throw runtime_error("unrecognized base " + string(1, base));
		}

		// TODO: Check if this is correct!!
		int distance_from_start = (ba.IsReverseStrand()) ? ba.Length - pa.PositionInAlignment - 1
				  	  	  	  	  	  	  	  	  	  	 : pa.PositionInAlignment;

		int distance_from_end = ba.Length - distance_from_start - 1;
		long double base_quality = (long double) pow(10, -0.1 * minQual );

		this->data.at(index).reads.push_back(Read(baseIdx, base_quality, distance_from_start,
				distance_from_end, ba.IsReverseStrand()));

		this->data.at(index).depth++;
		this->data.at(index).counts.total.at(baseIdx)++;

		if(ba.IsReverseStrand()) {
			this->data.at(index).counts.reverse.at(baseIdx)++;
		}
	}

	return;
}

bool MultisampleData::goodMinMaxDepth(const FilterSettings & filter_settings) {
	/* Quality control for normal: check minimum and maximum depth requirements */
    if(this->data.at(0).depth < filter_settings.min_depth
	   or this->data.at(0).depth >= filter_settings.max_depth)
		return false;

	for(size_t i = 0 ; i < this->data.size(); i++ ) {
		if (this->data.at(i).depth == 0) {
			return false;
		}
		total_depth += data.at(i).depth;
	}

	double mean_depth = double(total_depth) / data.size();
	if(mean_depth < filter_settings.min_depth) {
		return false;
	}
	return true;
}

bool MultisampleData::variationFound(const FilterSettings & filter_settings) {
	for(int i = 0; i < data.size(); i++) {
			if(count_variant_reads(this->data.at(i).counts.total) >= filter_settings.minVarAlleles) {
				return true;
			}
			/* This is to detect cases where no variant reads are found but the major frequency
			 * allele in tumour and normal don't match */
			else if(mismatchingReferences(this->data.at(i).counts.total, this->data.at(0).counts.total)) {
				return true;
			}
	    }
		return false;
}

bool mismatchingReferences(const vector <unsigned int> &tumour_counts, const vector <unsigned int > &normal_counts) {
	int referenceTumour = distance(tumour_counts.begin(), max_element(tumour_counts.begin(), tumour_counts.end()));
	int referenceNormal = distance(normal_counts.begin(), max_element(normal_counts.begin(), normal_counts.end()));
	return referenceTumour != referenceNormal;
}

bool MultisampleData::isSomaticCandidate(const FilterSettings & filter_settings) {
	if(goodMinMaxDepth(filter_settings) && variationFound(filter_settings)) {
		return true;
	}

	else
		return false;

}

void MultisampleData::callVariants(const GibbsSettings &gibbs_settings, const FilterSettings &filter_settings,
		const ProgramSettings &program_settings) {
  GibbsSampler inference_output = runGibbsSampler(gibbs_settings, program_settings, this);
  ResultsFactory results_factory = getResultsFactory(inference_output, this->pos.ref);
  Results* results = results_factory.create(program_settings.which_events, this->pos.ref);
  results->process(*this, inference_output, gibbs_settings, filter_settings, program_settings);
  results->extract_features(*this, inference_output, program_settings.features_file);
  delete results;
}

void MultisampleData::printMultiPileup() {
	cout << "Position " << this->pos.chrom         << ":" << this->pos.position << endl;
	cout << "Ref: "     << this->pos.ref << endl;
	string alleles = "ACGT";
	for(size_t i =0; i< this->data.size(); i++) {
		cout << "Sample: " << i << endl;

		for (size_t j =0; j < this->data.at(i).reads.size();j++) {
			cout << alleles[this->data.at(i).reads.at(j).base] << " " <<  this->data.at(i).reads.at(j).error_prob << " | ";

		}

		cout << endl;
		cout << "A: " << this->data.at(i).counts.total.at(0) << " | ";
		cout << "C: " << this->data.at(i).counts.total.at(1) << " | ";
		cout << "G: " << this->data.at(i).counts.total.at(2) << " | ";
		cout << "T: " << this->data.at(i).counts.total.at(3) << endl;
	}
}

int count_variant_reads(vector <unsigned int>& counts) {
	// Identify major base
	int majorBaseIdx = 0;
	for (int baseIdx = 0; baseIdx < 4; baseIdx++) {

		if (counts.at(baseIdx)
				 > counts.at(majorBaseIdx))
		{
			majorBaseIdx = baseIdx;
		}
	}

	// Identify minor base, initialize to base that is not the major base
	int minorBaseIdx = (majorBaseIdx + 1) % 4;
	for (int baseIdx = 0; baseIdx < 4; baseIdx++) {

		if (counts.at(baseIdx)
				 > counts.at(minorBaseIdx)
				 && baseIdx != majorBaseIdx )
		{
			minorBaseIdx = baseIdx;
		}
}
	return counts.at(minorBaseIdx);
}

bool is_normal_triallelic(const int threshold, const vector <unsigned int>& counts) {
	int alleles_found = 0;
	for (int i = 0 ; i < counts.size() ; i++) {
		if(counts.at(i) > threshold) // || counts.at(i) / accumulate(counts.begin(), counts.end(), 0.0) // TODO: If regr passes add this too.
				alleles_found++;
	}
	return alleles_found > 2;
}


