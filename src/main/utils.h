/*
 *  utils.h
 *  multisnv_proj
 *
 *  Created by Malvina Josephidou on 31/08/2014.
 *  Copyright 2014 University of Cambridge. All rights reserved.
 *
 */
#ifndef ____utils__
#define ____utils__
#include <iostream>
#include <string>
#include <vector>
#include<fstream>
#include<istream>
#include<sstream>
#include <cmath>
#include <stdio.h> //to use fopen etc
#include <vector>
#include <algorithm> // for copy
#include <iterator> // for ostream_iterator
#include <stdlib.h>
#include <map>
#include <utility>
#include <cstring>
#include <iterator>
#include "Settings.h"

using namespace std;
vector <int> addIntegerVectors(const vector <int> &counts1, const vector <int> &counts2);
template<typename T> T findMostFrequentElement(const vector <T> &counts) {
	return distance(counts.begin(),
					max_element(counts.begin(),counts.end()));
};
int base2ind(const char& base);
int base2ind(string base);
int index2intState(int initState);
string intState2string(int input_state);
int ref2state(char ref);
const vector<bool> string2Binary(string input_state);
vector<double> decimal2Binary(int input_state);
double compQual(const vector<int>& temp);
variant_type_t assignSomaticStatus(string reference, string normal, string other_sample);
bool isSomatic(string normal, string other_sample);
bool isLOH(string normal, string other_sample);
string intToString(int number);
#endif //utils.h
