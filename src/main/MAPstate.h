/*
 * MAPstate.h
 *
 *  Created on: Feb 24, 2015
 *      Author: joseph07
 */

#ifndef MAPSTATE_H_
#define MAPSTATE_H_

#include <vector>
#include <string>
using namespace std;

class MAPstate {
	private:
		int as_int;
		string as_str;
		long double qual;

	public:
		MAPstate() : as_int(0)
				   , as_str("")
				   {}
		~MAPstate();
		void init(vector <int> counts_per_state);
		int get_as_int() const;
		string get_as_str() const;
		long double getQual() const;

};

#endif /* MAPSTATE_H_ */
