/*
 * run_multisnv.h
 *
 *  Created on: Sep 20, 2014
 *      Author: malvinajosephidou
 */

#ifndef RUN_MULTISNV_H_
#define RUN_MULTISNV_H_

#include "api/BamMultiReader.h"
#include "Settings.h"
#include "GibbsSettings.h"

using namespace BamTools;

BamMultiReader prepare_reader(vector <string> bam_files);

void
run_multiSNV(BamMultiReader &reader, FilterSettings filter_settings, ProgramSettings program_settings,
				GibbsSettings gibbs_settings);

void run_on_all_regions(BamMultiReader &reader, const FilterSettings &filter_settings,
		const GibbsSettings &gibbs_settings, const ProgramSettings &program_settings);

void run_on_user_specified_regions(BamMultiReader &reader, const FilterSettings &filter_settings,
		const GibbsSettings &gibbs_settings, const ProgramSettings &program_settings);

void
run_on_BED_regions(BamMultiReader &reader, const FilterSettings &filter_settings,
		const GibbsSettings &gibbs_settings, const ProgramSettings &program_settings);

void run_multisnv_on_region(BamRegion region, string chrom,
		BamMultiReader &reader, const FilterSettings &filter_settings,
				const GibbsSettings &gibbs_settings, const ProgramSettings &program_settings);

bool isRegionOfInterest(const vector <string> & regions, string chrom);

BamRegion get_BamRegion_from_user_input(string region, const BamMultiReader &reader);


struct UserSpecifiedRegion {
	string chrom;
	vector <int> positions;
};

BamRegion get_BamRegion_from_user_input(UserSpecifiedRegion region, const BamMultiReader &reader);

UserSpecifiedRegion parse_user_input_region(string region);

#endif /* RUN_MULTISNV_H_ */
