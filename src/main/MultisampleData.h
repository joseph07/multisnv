/*
 * MultisampleData.h
 *
 *  Created on: Oct 28, 2014
 *      Author: joseph07
 */

#ifndef MULTISAMPLEDATA_H_
#define MULTISAMPLEDATA_H_

#include <vector>
#include <map>
#include <boost/interprocess/smart_ptr/unique_ptr.hpp>
#include "utils/bamtools_pileup_engine.h"

#include "Locus.h"
#include "Data.h"
#include "Settings.h"
#include "GibbsSettings.h"

using namespace std;
using namespace BamTools;

class MultisampleData {
public:
	static const string bases;

	Locus pos;
	vector <Data> data;

	int total_depth;
	int readsFailingQC;

	MultisampleData(size_t num);

	virtual ~MultisampleData();
	void printMultiPileup();
	void CreateMultiPileup(const PileupPosition& pileupData,
			const FilterSettings &filter_settings, map <string, int > bam_to_ind);
	void callVariants(const GibbsSettings &gibbs_settings, const FilterSettings &filter_settings,
		 const ProgramSettings &program_settings);
	bool isSomaticCandidate(const FilterSettings & filter_settings);
	bool goodMinMaxDepth(const FilterSettings & filter_settings);
	bool variationFound(const FilterSettings & filter_settings);
};

int count_variant_reads(vector <unsigned int>& counts);
bool is_normal_triallelic(const int threshold, const vector <unsigned int>& counts);
bool mismatchingReferences(const vector <unsigned int> &tumour_counts, const vector <unsigned int > &normal_counts);
#endif /* MULTISAMPLEDATA_H_ */
