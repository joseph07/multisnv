/*
 * Results.h
 *
 *  Created on: Feb 26, 2015
 *      Author: joseph07
 */

#ifndef RESULTS_H_
#define RESULTS_H_
#include "SampleResults.h"
#include "MultisampleData.h"
#include "GibbsSampler.h"
#define STRAND_BIAS_DEFAULT -1.0

bool is_failed_to_converge(int executed_cycles, int maxIter);
Counts pool_counts_of_mutated_samples(const SampleResults &normal_sample, const vector <SampleResults> &tumour_samples,
									  string mutated_state, const vector <Data> &data);

class Results {
protected:
	vector <SampleResults> tumour_samples;
	SampleResults normal_sample;
	variant_type_t event_type;
	bool failed_to_converge;
public:
	Results(SampleResults normal_sample_t,
			vector <SampleResults> tumour_samples_t,
			variant_type_t event_type_t)
					: tumour_samples(tumour_samples_t)
					, normal_sample(normal_sample_t)
					, event_type(event_type_t)
					{}
	virtual ~Results() { }
	virtual void process(const MultisampleData &ms_data, const GibbsSampler &inference_output, const GibbsSettings &gibbs_settings,
						 const FilterSettings & filter_settings, const ProgramSettings &program_settings);
	string get_all_allelic_compositions() const;
	/* This function does nothing unless the derived class ResultsReportSNV is called as
	 * some metrics are not computable for non variant
	 * or weird looking events! */
	virtual void extract_features(const MultisampleData &ms_data, const GibbsSampler &inference_output, string features_file) {}
};

struct FilterRelated {
	map <string, ReadDistance> startEndDist;
	bool areReadsClustered;
	bool isEvidenceWeak;
	bool isNormalContaminated;
	double normalContaminationFreq;
	double normalContaminationCounts;
	bool isTriallelic;
	double fractionFailingQC;
	double strand_bias;
	double variant_quality;
	double mean_depth;
	int depth_in_normal;
	string filter_flags;
	bool isHomopolymer;
	int longestHomopolymerRun;
	bool isNormalTriallelic;
	vector <double> variant_freqs;
	FilterRelated() : areReadsClustered(false),
					  isEvidenceWeak(false), isNormalContaminated(false), normalContaminationFreq(0.0),normalContaminationCounts(0),
					  isTriallelic(false), fractionFailingQC(0),
					  strand_bias(STRAND_BIAS_DEFAULT), variant_quality(0), mean_depth(0), depth_in_normal(0), filter_flags(""), isHomopolymer(false),
					  longestHomopolymerRun(0),  isNormalTriallelic(0)
					  {}
};

/* Abstract class */
class ReportedResults : public Results {
protected:
	char ref;
	string alternate_alleles;
	string normal_genotype;
	vector <string> tumour_genotypes;
	FilterRelated qual_control;

protected:
	void set_genotypes();
	void set_alternate_alleles(char ref);
	void set_variant_quality(const map <string, int > &countsPerAggregateState );
	void set_fraction_failingQC(const MultisampleData &ms_data);
	void set_mean_depth(const MultisampleData &ms_data);
	string get_more_alternate_alleles(string already_found_alleles, string state, char ref) const;
	string findNonUbiqAlleles();
	string findStateWithMostAlleles() const;
	virtual void apply_filters(const MultisampleData &ms_data, const FilterSettings &filter_settings, string fasta);
	void apply_filters(const MultisampleData &ms_data);
	void set_filter_flag(const FilterSettings& filter_settings);

public:
	virtual ~ReportedResults() { }
	typedef Results super;
	ReportedResults(SampleResults normal_sample_t,
						   vector <SampleResults> tumour_samples_t,
						   variant_type_t event_type_t,
						   char ref_t)
						   : super(normal_sample_t, tumour_samples_t, event_type_t)
						   , ref(ref_t) {
								set_alternate_alleles(ref);
								set_genotypes();
						   }
	virtual void process(const MultisampleData &ms_data, const GibbsSampler &inference_output,
						 const GibbsSettings &gibbs_settings, const FilterSettings &filter_settings, const ProgramSettings &program_settings);
	void appendVCF(string VCF, const MultisampleData &ms_data ) const;
	void print_result(const vector<Data>& data, const Locus &locus, const GibbsSampler &inference_output, bool doPrint) const;
	string get_alternare_alleles() const;
};


class ReportedResultsWildtype: public ReportedResults {
public:
	virtual ~ReportedResultsWildtype() { }
	typedef ReportedResults super;
	ReportedResultsWildtype(SampleResults normal_sample_t,
								   vector <SampleResults> tumour_samples_t,
								   variant_type_t event_type_t,
								   char ref_t)
								   : super(normal_sample_t, tumour_samples_t, event_type_t, ref_t)
								   {}

protected:
	virtual void apply_filters(const MultisampleData &ms_data, const FilterSettings &filter_settings, string fasta);
};

class ReportedResultsUnexpectedEvent: public ReportedResults {
public:
	virtual ~ReportedResultsUnexpectedEvent() { }
	typedef ReportedResults super;
	ReportedResultsUnexpectedEvent(SampleResults normal_sample_t,
								   vector <SampleResults> tumour_samples_t,
								   variant_type_t event_type_t,
								   char ref_t)
								   : super(normal_sample_t, tumour_samples_t, event_type_t, ref_t)
								   {}

protected:
	virtual void apply_filters(const MultisampleData &ms_data, const FilterSettings &filter_settings, string fasta);

};

/* Abstract class */
class ReportedResultsValidVariant : public ReportedResults {
public:
	virtual ~ReportedResultsValidVariant() { }
	typedef ReportedResults super;
	ReportedResultsValidVariant(SampleResults normal_sample_t,
								   vector <SampleResults> tumour_samples_t,
								   variant_type_t event_type_t,
								   char ref_t)
								   : super(normal_sample_t, tumour_samples_t, event_type_t, ref_t)
								   {}

protected:
	virtual void apply_filters(const MultisampleData &ms_data, const FilterSettings &filter_settings, string fasta);
	double get_strand_bias(const vector<Data>& data, const double &SB_threshold);
};


class ReportedResultsGermline: public ReportedResultsValidVariant {
public:
	virtual ~ReportedResultsGermline() { }
	typedef ReportedResultsValidVariant super;
	ReportedResultsGermline(SampleResults normal_sample_t,
								   vector <SampleResults> tumour_samples_t,
								   variant_type_t event_type_t,
								   char ref_t)
								   : super(normal_sample_t, tumour_samples_t, event_type_t, ref_t)
								   {}

protected:
	virtual void apply_filters(const MultisampleData &ms_data, const FilterSettings &filter_settings, string fasta);
	vector <double> compute_variant_freqs(const vector<Data> &data);
};


class ReportedResultsSomatic: public ReportedResultsValidVariant {
public:
	virtual ~ReportedResultsSomatic() { }
	typedef ReportedResultsValidVariant super;
	ReportedResultsSomatic(SampleResults normal_sample_t,
								   vector <SampleResults> tumour_samples_t,
								   variant_type_t event_type_t,
								   char ref_t)
								   : super(normal_sample_t, tumour_samples_t, event_type_t, ref_t)
								   {}

protected:
	virtual void apply_filters(const MultisampleData &ms_data, const FilterSettings &filter_settings, string fasta);
	bool are_reads_clustered(map <string, ReadDistance> &dist);
	bool is_homopolymer(const Locus &pos, string fasta_file, int homopol_threshold);
	int get_longest_homopolymer_run(const Locus &pos, string fasta_file, int homopol_threshold);
	double compute_max_variant_freq(const vector<Data> &data);
	bool is_evidence_weak(const vector <Data> &data, double weakEvidenceThreshold) ;
	map <string, ReadDistance> getStartEndReadDistances(const vector <Data>& data);
	virtual void extract_features(const MultisampleData &ms_data, const GibbsSampler &inference_output, string features_file);
	vector <double> compute_variant_freqs(const vector<Data> &data);
};

class ReportedResultsSNV: public ReportedResultsSomatic {
public:
	virtual ~ReportedResultsSNV() { }
	typedef ReportedResultsSomatic super;
	ReportedResultsSNV(SampleResults normal_sample_t,
							   vector <SampleResults> tumour_samples_t,
							   variant_type_t event_type_t,
							   char ref_t)
							   : super(normal_sample_t, tumour_samples_t, event_type_t, ref_t)
							   {}

protected:
	virtual void apply_filters(const MultisampleData &ms_data, const FilterSettings &filter_settings, string fasta);
	bool is_normal_contaminated(const Data& normal_data, double threshold);
	void set_is_normal_contaminated(const Data& normal_data, double threshold);
	void set_normal_contamination(const Data& normal_data);

};

class ReportedResultsLOH: public ReportedResultsSomatic {
public:
	virtual ~ReportedResultsLOH() { }
	typedef ReportedResultsSomatic super;
	ReportedResultsLOH(SampleResults normal_sample_t,
							   vector <SampleResults> tumour_samples_t,
							   variant_type_t event_type_t,
							   char ref_t)
							   : super(normal_sample_t, tumour_samples_t, event_type_t, ref_t)
							   {}

protected:
	virtual void apply_filters(const MultisampleData &ms_data, const FilterSettings &filter_settings, string fasta);

};

#endif /* RESULTS_H_ */
