/*
 * PileupQueue.cpp
 *
 *  Created on: Oct 23, 2014
 *      Author: joseph07
 */
#include <numeric>

#include "PileupQueue.h"
#include "MultisampleData.h"
#include "GibbsSettings.h"
#include "SummaryResults.h"

void printProgress(string chrom, int position) {
	static int count = 1;
	count ++;
	if(count % 100000 == 0) {
		cout << endl;
		cout << "Progress meter " << chrom << ": " << position << endl;
	}
}

void PileupQueue::processPileupData(const PileupPosition &pileupData) {
	MultisampleData ms_data(bam_to_ind.size());
	ms_data.CreateMultiPileup(pileupData, filter_settings, this->bam_to_ind);

	printProgress(this->chrom, pileupData.Position);

	if(ms_data.isSomaticCandidate(filter_settings)) {
		ms_data.pos.fill(program_settings.ref_fasta, chrom, pileupData.Position);
		if(ms_data.pos.ref != 'N') {
			ms_data.callVariants(gibbs_settings, filter_settings, program_settings);
		}
	}
	return;
}

void PileupQueue::Visit(const PileupPosition& pileupData) {

	/* TODO: Should code even be reaching this point if no alignments have been found? */
	if(pileupData.PileupAlignments.empty()) {
		return;
	}
	/* Reset if we ended up on the wrong chromosome */
	if (pileupData.RefId != -1 && pileupData.RefId != RefId) {
		return;
	}

	/* Ignore positions out of location of interest */
	if (pileupData.Position < StartPosition
			|| (pileupData.Position >= StopPosition && StopPosition != -1)) {
		return;
	}

	try {
		this->processPileupData(pileupData);
		SummaryResults::getInstance()->n_sites++;
	}

	catch(runtime_error &e) {
		cerr << e.what() << endl;
	}
}

void PileupQueue::Clear() {
}
