
/*
 *  Locus.h
 *  multisnv_proj
 *
 *  Created by Malvina Josephidou on 25/08/2014.
 *  Copyright 2014 University of Cambridge. All rights reserved.
 *
 */
#ifndef ____Locus__
#define ____Locus__
#include <iostream>
#include <string>
#include <vector>
#include<fstream>
#include<istream>
#include<sstream>
#include <cmath>
#include <stdio.h> //to use fopen etc
#include <vector>
#include <algorithm> // for copy
#include <iterator> // for ostream_iterator
#include <stdlib.h>
#include <map>
#include <utility>
#include <cstring>
#include <iterator>
#include <map>

using namespace std;
bool is_valid_ref(char base);
int getFastaRefId(string ref_fasta, string chrom);

class Locus {
public:
	string chrom;
	char ref;
	int RefId;
	int position;
	char getRefFromFasta(string fasta_filename);
	void fill(string ref_fasta, string chrom, int position);
	bool is_homopolymer(string ref_fasta, int threshold, string alt_allele) const;
	int get_longest_homopolymer_run(string ref_fasta, int threshold, string alt_allele) const;
	
	Locus() :
		chrom(""),
		ref('A'),
		RefId(-1),
		position(-1)
	{}
};

#endif /* defined(____Locus__) */
