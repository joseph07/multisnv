/*
 * ReadDistance.h
 *
 *  Created on: Feb 18, 2015
 *      Author: joseph07
 */

#ifndef READDISTANCE_H_
#define READDISTANCE_H_

#include <iostream>
#include <vector>
#include <stdexcept>
#include <algorithm>
using namespace std;

double compute_median_from_input(vector <int> numbers);

class ReadDistance {
	public:
		ReadDistance();
		ReadDistance(vector <int> input_dist);
		vector <int> distance;

		double getMedian();
		double getMedianAbsoluteDeviation();
		void compute_median_absolute_deviation();
		void compute_median();

private:
	double median;
	double median_absolute_deviation;

};

#endif /* READDISTANCE_H_ */
