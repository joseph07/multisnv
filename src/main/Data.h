/*
 * Data.h
 *
 *  Created on: Oct 24, 2014
 *      Author: joseph07
 */

#ifndef DATA_H_
#define DATA_H_
#include <iostream>
#include <vector>

#include "ReadDistance.h"
#include "Read.h"
using namespace std;

class Counts {
public:
    Counts() : total(4,0)
    	   	 , reverse(4,0)
    	   	   {}
     vector <unsigned int> total;
	 vector <unsigned int> reverse;
	 vector <unsigned int> get_forward();
	 Counts operator+(const Counts &counts);
};

class Data {
public:
	Data();

    int depth;
    Counts counts;
	vector <Read> reads;
};




#endif /* DATA_H_ */
