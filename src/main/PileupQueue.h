/*
 * PileupQueue.h
 *
 *  Created on: Oct 23, 2014
 *      Author: joseph07
 */

#ifndef PILEUPQUEUE_H_
#define PILEUPQUEUE_H_
#include "api/BamWriter.h"
#include <iostream>
#include "api/BamMultiReader.h"
#include "api/BamReader.h"
#include <stdlib.h>
#include <vector>
#include "utils/bamtools_pileup_engine.h"
#include "fasta.h"
#include "utils/bamtools_utilities.h"
#include "utils/bamtools_options.h"
#include "api/BamAlignment.h"
#include "api/BamAux.h"
#include <cmath>
#include <queue>
#include <map>

#include "Settings.h"
#include "Data.h"
#include "MultisampleData.h"

using namespace std;
using namespace BamTools;

struct PileupQueue : PileupVisitor
{
	map <string, int> bam_to_ind;
	int RefId;
	string chrom;
	int StartPosition;
	int StopPosition;
	ProgramSettings program_settings;
	FilterSettings filter_settings;
	GibbsSettings gibbs_settings;

	PileupQueue(int refId, string chr, int start, int stop ,const FilterSettings &fr_settings, const vector <string>& bamFiles,
			const GibbsSettings& gs_settings, const ProgramSettings &pr_settings)
		: RefId(refId)
		, chrom(chr)
		, StartPosition(start)
		, StopPosition(stop)
		, filter_settings(fr_settings)
		, gibbs_settings(gs_settings)
		, program_settings(pr_settings) {

		for(size_t i = 0; i < bamFiles.size(); i++) {
			bam_to_ind[bamFiles.at(i)] = i;
		}
	}

	void Visit(const PileupPosition& pileupData);
	void processPileupData(const PileupPosition &pileupData);
	void Clear();

};


#endif /* PILEUPQUEUE_H_ */
