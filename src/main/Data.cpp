/*
 * Data.cpp
 *
 *  Created on: Oct 24, 2014
 *      Author: joseph07
 */


#include <iostream>
#include "fasta.h"

#include "Data.h"

using namespace std;
using namespace BamTools;

Data::Data()
	   : depth(0)
       {}

vector <unsigned int> Counts::get_forward() {
		 vector <unsigned int > forward(total.size(), 0);
		 for(int i = 0 ; i < total.size() ; i++) {
			 forward.at(i) = total.at(i) - reverse.at(i);
		 }
		 return forward;
}

Counts Counts::operator+(const Counts &counts) {
	Counts total_counts;
	for(int ii = 0; ii < 4; ii++) {
		total_counts.total.at(ii) = total.at(ii) + counts.total.at(ii);
		total_counts.reverse.at(ii) = reverse.at(ii) + counts.reverse.at(ii);
	}
	return total_counts;
}
