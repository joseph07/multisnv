//
//  rungibbs.cpp
//  
//
//  Created by Malvina Josephidou on 06/02/2013.
//
//

#include <iostream>
#include <vector>
#include <iomanip>
#include <string>
#include <cstring>
#include<fstream>
#include<istream>
#include<string>
#include<sstream>
#include <algorithm> // for copy
#include <iterator> // for ostream_iterator
#include <stdlib.h>
#include <map>
#include <string.h>
#include <utility>
#include <cstring>
#include <iterator>
#include<sstream>
#include <cmath>
#include <stdio.h> //to use fopen etc
#include <algorithm> // for copy
#include <iterator> // for ostream_iterator
#include <stdlib.h>
#include <cstdlib>
#include <ctime>
#include <numeric>
#include <functional>
#include <boost/math/distributions/hypergeometric.hpp>
#include <stdexcept>
#include <limits>

using namespace boost::math;
using namespace std;

#include "utils.h"
#include "GibbsSampler.h"
#include "rungibbs.h"
#include "Gibbs.h"
#include "TumourSample.h"
#include "ConditionalDistr.h"

vector <int> getValuesFromRandomInitialization(const vector <Gibbs*> &ptr_gibbs) {
	vector <int> mostRecentGibbsOutput;
	for(int i = 0; i < ptr_gibbs.size(); i++) {
		mostRecentGibbsOutput.push_back(ptr_gibbs.at(i)->samples.back());
	}
	return mostRecentGibbsOutput;
}

void GibbsSampler::drawSamples(const GibbsSettings &gibbs_settings,
		boost::ptr_vector<ConditionalDistr> &conditional_distr )
{
    int sample = 0;
    vector <int> latest_drawn_states = getValuesFromRandomInitialization(samplerHistory);

    map < AllelicState, map <AllelicState, long double> > beta_hyperp
    					= ConditionalTumourDistr::getBeta(gibbs_settings.mutrate);
    AllelicState reference = AllelicState(ref2state(ConditionalDistr::getRef()));
    map <AllelicState, long double > theta = ConditionalNormalDistr::getTheta(reference);

	for( int zz = 0; zz < gibbs_settings.gibbs_runs; zz++ ) {
         for(int x = 0 ; x < numberOfParameters; x++) {
        	 	conditional_distr.at(x).updatePosterior(x, theta, latest_drawn_states, beta_hyperp, gibbs_settings.weight);
                conditional_distr.at(x).updateFixedStates(latest_drawn_states);
                /* Save this version of previousSamples - this is what we used tog generate probs for x.*/
                sample = conditional_distr.at(x).sample_from_conditional();
                latest_drawn_states.at(x) = sample; //Update with new sample.
                samplerHistory.at(x)->samples.push_back(sample);
            }
	}
    return;
}


bool check_if_priors_changed( const vector <int> &current, 
							 const vector <int> &previous) {
	
	if(current.empty() || previous.empty() )
		return true;
	
	else {
          return (equal(current.begin(),
						current.end(), previous.begin()) == true ? false : true); 
	}
}

int evolutionary_distance(const vector <bool> &tumour_state, const vector <bool> &normal_state) {
	int n = 0;
	for (int i = 0 ; i < 4 ; i++) {
		if(tumour_state.at(i) != normal_state.at(i)) {
			n++;
		}
	}
	return n;
}





