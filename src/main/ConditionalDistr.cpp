/*
 * ConditionalDistr.cpp
 *
 *  Created on: Sep 20, 2014
 *      Author: malvinajosephidou
 */


#include <numeric>
#include <cassert>
#include <cmath>
#include "rungibbs.h"
#include "ConditionalDistr.h"
#include "NormalSample.h"
#include "TumourSample.h"
#include "Read.h"
char
ConditionalDistr::ref = 'N';

void ConditionalDistr::setRef(char input) {
	ref = input;
}
char ConditionalDistr::getRef() {
	return ref;
}

ConditionalDistr*
ConditionalDistr::createConditionalDistr(bool is_normal) {
	if (is_normal)
		return new ConditionalNormalDistr;

	else
		return new ConditionalTumourDistr;
}

const vector <long double>&
ConditionalDistr::getLikelihood() {
	return likelihood;
}

void
ConditionalDistr::set_likelihood_hyperparameter(const int sample_depth,
		int MedianCoverage, long double alpha)
{
	likelihood_hyperparameter = (MedianCoverage < sample_depth)
							  ? (alpha *  sample_depth )
									  /MedianCoverage
						      : alpha;
}

void
ConditionalDistr::updatePosterior(int x, const map <AllelicState, long double> &thetas, const vector <int> &latest_drawn_states,
		const map < AllelicState, map <AllelicState, long double> > &beta_hyperp, long double weight)
{
	/* latest_drawn states are used to derive prior estimates for the conditional distr of x.
	 * if latest_drawn_states are the same as the states used to get the posterior for x
	 *  during the previous run -do not update them! */
	bool update_required = check_if_priors_changed(latest_drawn_states,
			this->getFixedStates());

	if(update_required) {
		vector <long double> priors = this->getPriors(latest_drawn_states, weight, x, thetas, beta_hyperp);
		vector <long double> log_probs;
		for(size_t i = 0; i < this->getAllelicStates().size(); i++) {
			log_probs.push_back (this->getLikelihood().at(i) + log(priors.at(i)));
		}

		this->setPosterior(safe_exponentiation(log_probs));
	}
	return;
}

long double
ConditionalDistr::get_likelihood_hyperparameter()
{
	return likelihood_hyperparameter;
}

void
ConditionalDistr::set_likelihood( const Data& data)
{	
	for(size_t j = 0; j < allelic_states.size() ; j ++)
	{
		int total_reads = allelic_states.at(j).total_reads_matching_alleles_in_state(data.counts.total);

		int n_alleles = allelic_states.at(j).getAlleles().size();
		double denominator  = (double)
										total_reads +
										(n_alleles * get_likelihood_hyperparameter()) -
										n_alleles;

		vector <double> allelic_fraction_estimate(4, 0.0);
		for(int i = 0; i < 4 ; i++) {
			if(allelic_states.at(j).getIs_allele_present().at(i)) {
				allelic_fraction_estimate.at(i) =
						(data.counts.total.at(i)
								+ get_likelihood_hyperparameter() - 1.0)
								/(denominator);
			}
		}

		try {
			if (fabs(1.0 - accumulate(allelic_fraction_estimate.begin(),
					allelic_fraction_estimate.end(), 0.0)) > EPSILON)
			{
				cout << fabs(1.0 - accumulate(allelic_fraction_estimate.begin(),
						allelic_fraction_estimate.end(),0.0)) << endl;
				throw "Allelic fractions do not add up to 1.0!" ;
			}
		}

		catch(const char* msg) {
			cerr << msg << endl;
			exit(EXIT_FAILURE);
		}

		AllelicState current_state = allelic_states.at(j);
		long double logprob = 0;

		for(size_t i = 0; i < data.reads.size() ; i++) {

			int ind = data.reads.at(i).base;
			long double qual = data.reads.at(i).error_prob;

			logprob = current_state.getIs_allele_present().at(ind) == true
					? logprob + log(
							((1-qual) * allelic_fraction_estimate.at(ind)) +
							((qual/3) * (1-allelic_fraction_estimate.at(ind)))
					)
					: logprob + log(qual/3);
		}
		try {
			if(exp(logprob) < 0) {
				throw "Invalid zero likelihood !" ;
			}
		}

		catch(const char* msg) {
			cerr << msg << endl;
			exit(EXIT_FAILURE);
		}

		/* BIC penalty */
		logprob = logprob - 0.5 * n_alleles* log(data.reads.size());
		likelihood.push_back(logprob);
	}
	assert(likelihood.size() == allelic_states.size());
}

int ConditionalDistr::sample_from_conditional()
{
    long double random_number;
    int sample;

    random_number = rand() / (long double) RAND_MAX;
    long double low_int=0.0;
    long double high_int = posterior.at(0);
    sample = allelic_states.at(0).getInteger();

    for(int i = 0; random_number > low_int
    				&& random_number > high_int + EPSILON; i++)
    {
		low_int = high_int;
		high_int = high_int + posterior.at(i+1);
        sample = allelic_states.at(i+1).getInteger();
    }

    return sample;
}

const vector<long double> &ConditionalDistr::getPosterior() const
{
    return posterior;
}

void ConditionalDistr::setPosterior(vector<long double> posterior)
{
    this->posterior = posterior;
}

const vector<AllelicState> &ConditionalDistr::getAllelicStates() const
{
    return allelic_states;
}

void ConditionalDistr::setAllelicStates(const vector<AllelicState> &allelic_states)
{
    this->allelic_states = allelic_states;
}

const vector<int> &ConditionalDistr::getFixedStates() const
{
    return fixed_states;
}

void ConditionalDistr::updateFixedStates(vector<int> fixed_parameters)
{
    this->fixed_states = fixed_parameters;
}









