/*
 * run_multisnv.cpp
 *
 *  Created on: Sep 20, 2014
 *      Author: malvinajosephidou
 */
#include "run_multisnv.h"

#include <iostream>
#include <fstream>
#include <istream>
#include <iomanip>
#include <string>
#include <sstream>
#include <stdexcept>
#include <cmath>
#include <stdio.h>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <iterator>
#include <map>
#include <time.h>
#include <utility>
#include <cstring>
#include <functional>
#include <numeric>
#include <errno.h>
#include <unistd.h>
#include <ctype.h>
#include <stdlib.h>

#include "api/BamMultiReader.h"
#include "api/BamAlignment.h"
#include "api/BamAux.h"
#include "utils/bamtools_pileup_engine.h"

#include "Settings.h"
#include "GibbsSettings.h"
#include "rungibbs.h"
#include "multisnv_input_parser.h"
#include "write_snp_header.h"
#include "PileupQueue.h"

using namespace std;
using namespace BamTools;

BamMultiReader prepare_reader(vector <string> bam_files) {
	BamMultiReader reader;
	if(!reader.Open(bam_files)) {
		cerr << reader.GetErrorString() << endl;
		throw runtime_error("Could not open BAM files");
	}

	/* Create indexes if there are none */
	if(!reader.LocateIndexes()) {
		cerr << "Failed to find index for all bam files...Indexing.." << endl;
		reader.CreateIndexes(BamIndex::STANDARD);
	}
	return reader;
}

void run_multiSNV(BamMultiReader &reader , FilterSettings filter_settings, ProgramSettings program_settings,
		GibbsSettings gibbs_settings) {
		if(!program_settings.bed_file.empty()) {
			run_on_BED_regions(reader, filter_settings, gibbs_settings, program_settings);
		}

		else if(!program_settings.regions.empty()) {
			run_on_user_specified_regions(reader, filter_settings, gibbs_settings, program_settings);
		}

		else {
			run_on_all_regions(reader, filter_settings, gibbs_settings, program_settings);
		}
}

/* Runs if no regions and no bed specified: defaults to here */
void run_on_all_regions(BamMultiReader &reader, const FilterSettings &filter_settings,
		const GibbsSettings &gibbs_settings, const ProgramSettings &program_settings) {

	cout << "multiSNV will run on all regions \n" << endl;

	RefVector refs = reader.GetReferenceData();

	for(size_t i = 0; i < refs.size() ; i++) {
		string chrom = refs.at(i).RefName;
		int refId = reader.GetReferenceID(refs.at(i).RefName);
		int StartPosition = 1;
		int StopPosition = refs.at(i).RefLength;  // Scan til end of chromosome

		cout << "Running multiSNV on chromosome " <<  chrom << " " << StartPosition << ": " << StopPosition << '\n' << endl;
		BamRegion region(refId, StartPosition, refId, StopPosition);

		run_multisnv_on_region(region, chrom, reader, filter_settings, gibbs_settings, program_settings);
	}
	return;
}

void run_multisnv_on_region(BamRegion region, string chrom,
		BamMultiReader &reader, const FilterSettings &filter_settings,
		const GibbsSettings &gibbs_settings, const ProgramSettings &program_settings) {

	if (!reader.SetRegion(region) ) {
		throw runtime_error("Could not set region ");
	}

 	PileupEngine* m_PileupEngine = new PileupEngine();
 	//Introduce a -1 offset on start and end as bamtools is 0-based. This means if we want to run on pos 127, we need to tell bamtools to look at alignments starting at 126
	PileupQueue* m_PileupQueue = new PileupQueue(region.LeftRefID, chrom, region.LeftPosition-1, region.RightPosition-1,
			filter_settings, program_settings.bam_files, gibbs_settings, program_settings);

	m_PileupEngine->AddVisitor(m_PileupQueue);

	BamAlignment ba;
	while (reader.GetNextAlignment(ba)) {
		m_PileupEngine->AddAlignment(ba);
	}

	m_PileupEngine->Flush();
	delete m_PileupEngine;
	delete m_PileupQueue;
	return;
}


void run_on_user_specified_regions(BamMultiReader &reader, const FilterSettings &filter_settings,
		const GibbsSettings &gibbs_settings, const ProgramSettings &program_settings) {

	cout << "multiSNV will call variants on regions specified using --regions " << endl;
	cout << endl;
	for( int i = 0 ; i < program_settings.regions.size() ; i++) {
		UserSpecifiedRegion user_region = parse_user_input_region(program_settings.regions.at(i));
		BamRegion region = get_BamRegion_from_user_input(user_region, reader);

		run_multisnv_on_region(region, user_region.chrom, reader,
					filter_settings, gibbs_settings, program_settings);
	}
	return;
}

UserSpecifiedRegion parse_user_input_region(string region) {
	UserSpecifiedRegion parsed_region;
	char* pch;
	pch = strtok(&region[0],"-:");

	if(pch != NULL) {
		parsed_region.chrom = pch;
	}

	while(pch != NULL) {
		pch = strtok(NULL, "-:");
		if(pch != NULL) {
			parsed_region.positions.push_back(atoi(pch));

			if(parsed_region.positions.back() < 0) {
				throw overflow_error("Integer overflow");
			}
		}
	}
	return parsed_region;
}

BamRegion get_BamRegion_from_user_input(UserSpecifiedRegion region, const BamMultiReader &reader) {

	RefVector refs = reader.GetReferenceData();
	for ( int i = 0 ; i < refs.size(); i++) {
		if(refs.at(i).RefName == region.chrom) {
			if(region.positions.empty()) {
				region.positions.push_back(1);
				region.positions.push_back(refs.at(i).RefLength);  // Scan til end of chromosome
			}

			else if(region.positions.size() == 1 ) {
				region.positions.push_back(refs.at(i).RefLength);
			}

			int refId = reader.GetReferenceID(refs.at(i).RefName);

			BamRegion bam_region(refId, region.positions.at(0), refId, region.positions.at(1));
			cout << "Running multiSNV on " << region.chrom << ":" << region.positions.at(0) << "-" << region.positions.at(1) << endl;
			cout << endl;
			return bam_region;
		}
	}
	throw runtime_error("Could not set BAM region from user input. "
			"Check that the input to -regions is compatible with the reference names.");
}

void run_on_BED_regions(BamMultiReader &reader, const FilterSettings &filter_settings,
		const GibbsSettings &gibbs_settings, const ProgramSettings &program_settings)
{
	cout << "Running multiSNV on regions listed in BED file only..." << endl;
	cout << endl;

	FILE* bedFile = fopen(program_settings.bed_file.c_str(), "r");
	if (bedFile == NULL) {
		throw runtime_error("Error opening BED file! ");
	}

	else if(bedFile != NULL) {
		char line_read[BUFFER_LENGTH_LINE];

		while ( ! feof (bedFile)) {
			while ( fgets (line_read , BUFFER_LENGTH_LINE , bedFile) != NULL ) {

				// TODO: Parse line : Need to make some tests here. Full bed file can lead to buffer overflow!
				char* pch = strtok (line_read," \t \n ");
				if(pch == NULL) {
					throw runtime_error("Unexpected input from BED file..");
				}
				string chrom = string(pch);
				pch = strtok (NULL," \t \n ");
				if(pch == NULL) {
					throw runtime_error("Unexpected input from BED file..");
				}

				int StartPosition = atoi(pch);

				pch = strtok (NULL," \t \n ");

				if(pch == NULL) {
					throw runtime_error("Unexpected input from BED file..");
				}
				int StopPosition = atoi(pch);
				int refId = reader.GetReferenceID(chrom);
				if(refId == -1) {
					/* refId is set to -1 if we can't get referenceId for chromosome */
					cout << "Could not set the bam reader to this region specified in the Bed file: " << endl;
					cout << chrom << '\t' << StartPosition << '\t' << StopPosition << endl;
					cout << endl;
					continue;
				}
				BamRegion region(refId, StartPosition, refId, StopPosition);
				run_multisnv_on_region(region, chrom, reader, filter_settings, gibbs_settings, program_settings);
			}
		}
	}
	return;
}



