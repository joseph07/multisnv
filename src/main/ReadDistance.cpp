/*
 * ReadDistance.cpp
 *
 *  Created on: Feb 18, 2015
 *      Author: joseph07
 */

#include "ReadDistance.h"
#include <cmath>
#include <cassert>

using namespace std;

ReadDistance::ReadDistance() : median(-1)
							 , median_absolute_deviation(-1)
							 {}

ReadDistance::ReadDistance(vector <int> input_dist)
							 : median(-1)
							 , median_absolute_deviation(-1)
							 , distance(input_dist) {
								compute_median();
								compute_median_absolute_deviation();
							 }

double ReadDistance::getMedianAbsoluteDeviation() {
	return median_absolute_deviation;
}

double ReadDistance::getMedian() {
	return median;
}


void ReadDistance::compute_median() {
	try {
		this->median = compute_median_from_input(this->distance);
	}

	catch (domain_error &e) {
		cout << e.what() << endl;
	}
}

void ReadDistance::compute_median_absolute_deviation() {
	assert(this->median != -1);
	vector <int> absolute_deviation(this->distance.size(),0);

	for(int i = 0 ; i < this->distance.size() ; i++) {
		absolute_deviation.at(i) = abs(this->distance.at(i) - this->median);
	}
	try {
		this->median_absolute_deviation = compute_median_from_input(absolute_deviation);
	}

	catch (domain_error &e) {
		cout << e.what() << endl;
	}
}

double compute_median_from_input(vector <int> numbers) {
	typedef vector <int> ::size_type vec_size;
	vec_size size = numbers.size();

	if(size == 0) {
		throw domain_error("Should not be trying to find median of empty vector");
	}

	else {
		sort(numbers.begin(), numbers.end());
		vec_size mid = size/2;

		double median = size % 2 == 0 ? 0.5 * (numbers.at(mid-1) + numbers.at(mid))
							 : numbers.at(mid);
		return median;
	}
}
