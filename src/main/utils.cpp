/*
 *  utils.cpp
 *  multisnv_proj
 *
 *  Created by Malvina Josephidou on 31/08/2014.
 *  Copyright 2014 University of Cambridge. All rights reserved.
 *
 */

#include "utils.h"
#include <cassert>

int base2ind(const char& base) {
    int ind = 0;
    switch(base) {
        case 'A':
        case 'a':
            ind = 0;
            break;     
        case 'C':
        case 'c':
            ind = 1;
            break;    
        case 'G':
        case 'g':
            ind = 2;
            break;    
        case 'T':
        case 't':
            ind = 3;
            break;  
        default:
            cerr << "Unknown allele detected " << endl;
            exit(EXIT_FAILURE);
    }
    return ind;
}

int base2ind(string base) {
  assert(base.size() == 1 );
  return base2ind(base[0]);
}


vector <int> addIntegerVectors(const vector <int> &counts1, const vector <int> &counts2) {
	 assert(counts1.size() == counts2.size());
	 vector <int> total(counts1.size());
	 for(int j = 0; j < total.size(); j++) {
		            total.at(j) = counts1.at(j) + counts2.at(j);
	 }
	 return total;
}

int index2intState(int initState) {
    int pivot = 0;
    switch (initState) {
		case 0:
            pivot = 8;
            break;
		case 1:
            pivot = 4;
            break;
		case 2:
            pivot = 2;
            break;
		case 3:
            pivot = 1;
            break;
		default:
            cerr << "Invalid index to ACGT found: " << initState << endl;
            exit(EXIT_FAILURE);
    }
    return pivot;
}

string intState2string(int input_state) {
	string bases = "ACGT";
	string stringState ="";
	vector <double > binaryState = decimal2Binary(input_state);
	for(int i = 0; i < 4; i++) {
		if(binaryState.at(i) != 0) {
			stringState = stringState + bases[i];
		}
	}
	return stringState;
}


/* TODO Fix this and int2string, this function should return a const vector <bool>,
 * use function from AllelicStates.cpp */

vector<double> decimal2Binary(int input_state) {
    int index = 3;
    vector <double> binaryState(4,0.0);
        while(input_state >= 1) {
            binaryState[index] = double(input_state % 2);
            input_state = input_state / 2;
            index--;
        }
    return binaryState;
}

const vector<bool> string2Binary(string input_state) {
    vector <bool> binaryState(4,0.0);
    for(int i = 0 ; i < input_state.size(); i++) {
    	int ind = base2ind(input_state.at(i));
    	binaryState.at(ind) = true;
    }
    return binaryState;
}

int ref2state(char ref) {
    int pivot = 0;
    switch (ref) {
		case 'A':
            pivot = 8;
            break;
		case 'C':
            pivot = 4;
            break;
		case 'G':
            pivot = 2;
            break;
		case 'T':
            pivot = 1;
            break;
		default:
            cerr << "What is this " << ref << endl;
            exit(EXIT_FAILURE);
    }
    return pivot;
}

double compQual(const vector<int>& temp) {
    int first = 0;
    int second = 0;
    int tempNum;

    for(int i = 0; i < temp.size(); i++) {
        if(temp.at(i) > second) {
            second = temp.at(i);
        }

        if(second > first) {
            tempNum = second;
            second = first;
            first = tempNum;
        }
    }

    if(second == 0) {
    	return 100; /* don't want to return inf as this causes problems when reading vcf file */
    }

    else if (second == first)
    	return 0;

    else {
    	return  -10.0 * log10(double(second)/first);
    }
}

/* TODO: Eventually move to SampleResults.h class */
variant_type_t assignSomaticStatus(string reference, string normal, string other_sample) {
	//assert(reference.size() == 1 && reference != 'N');
		if(other_sample == normal)
			return (normal == reference) ? WILDTYPE : GERMLINE;
		else {
			if(isLOH(normal, other_sample))
				return LOH;
			else if(isSomatic(normal, other_sample))
				return SOMATIC;
			else
				return UNEXPECTED;
		}
}

bool isLOH(string normal, string other_sample) {
	if(normal.size() > other_sample.size())
		 return (normal.find_first_of(other_sample) != string :: npos) ? true : false;
	else
		return false;
}

bool isSomatic(string normal, string other_sample) {
	if(other_sample.size() - normal.size() == 1) {
		 return (other_sample.find_first_of(normal) != string :: npos) ? true : false;
	}
	else
		return false;
}


string intToString(int number)
{
   stringstream ss;//create a stringstream
   ss << number;//add number to the stream
   return ss.str();//return a string with the contents of the stream
}
