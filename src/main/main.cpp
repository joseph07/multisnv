
//  
//
//  Created by Malvina Josephidou on 05/02/2013.

#include <iostream>
#include <fstream>
#include <istream>
#include <iomanip>
#include <string>
#include <sstream>
#include <cmath>
#include <stdio.h> 
#include <vector>
#include <algorithm> 
#include <iterator> 
#include <map>
#include <time.h>
#include <utility>
#include <cstring>
#include <functional>
#include <numeric>
#include <boost/regex.hpp>
#include <boost/program_options.hpp>
#include <errno.h>
#include <unistd.h>
#include <ctype.h>
#include <stdlib.h>
#include "api/BamMultiReader.h"
#include "api/BamAlignment.h"
#include "utils/bamtools_pileup_engine.h"
#include "api/BamAux.h"

#include "Settings.h"
#include "GibbsSettings.h"
#include "rungibbs.h"
#include "multisnv_input_parser.h"
#include "run_multisnv.h"
#include "SummaryResults.h"
#include "write_snp_header.h"
#include "kml2mapsVersion.h"

namespace po = boost::program_options;
using namespace std;

int
main(int argc, char *argv[]) {
	clock_t start = clock();

	FilterSettings filter_settings;
	ProgramSettings program_settings;
	GibbsSettings gibbs_settings;

    po::variables_map vm;
    const po::options_description
    desc = get_multisnv_options_desc (filter_settings,
    		program_settings, gibbs_settings);
	
	try {
		po::store(po::parse_command_line(argc, argv, desc), vm);
		po::notify(vm);
		if ((argc == 1) or vm.count("help")) {
			cout << desc << "\n";
			return 0;
		}

		else if(vm.count("version")) {
			cout << "multiSNV " << GIT_REVISION << endl;
			return 0;
		}
    }
    
	catch(po::error& e) { 
		cerr << "ERROR: " << e.what() << endl; /* TODO: Add logging */
		cerr << desc      << endl;
		return 1; 
    }

    finalise_settings(vm, filter_settings,
    		program_settings, gibbs_settings);

    srand(program_settings.seed);

    try {
        BamMultiReader reader = prepare_reader(program_settings.bam_files);
    	write_VCF_header(program_settings.filename_output, filter_settings, program_settings, gibbs_settings, argc, argv);
    	if(program_settings.features_file != "") {
    		write_VCF_header(program_settings.features_file, filter_settings, program_settings, gibbs_settings, argc, argv);
    	    write_features_file_header(program_settings.features_file, program_settings.NumberOfSamples);
    	}
        run_multiSNV(reader, filter_settings, program_settings, gibbs_settings);
    }

    catch (runtime_error &e) {
    	cerr << "Exception caught: " << e.what() <<  endl;
    	return 1;
    }

    double runtime = (double) (clock() - start) / CLOCKS_PER_SEC;
    SummaryResults::getInstance()->print_summary(runtime);
    SummaryResults::destroyInstance(); /* will destroy singleton if it has been instantiated */

	return 0;
}



