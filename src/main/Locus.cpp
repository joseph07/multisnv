/*
 * Locus.cpp
 *
 *  Created on: Nov 1, 2014
 *      Author: joseph07
 */

#include "Locus.h"
#include "utils.h"
#include "fasta.h"
#include <stdexcept>
#include <cassert>
using namespace BamTools;
using namespace std;

char Locus::getRefFromFasta(string fasta_filename) {
	FastaE fasta_file;

	if(!fasta_file.Open(fasta_filename, fasta_filename + ".fai")) {
		exit(EXIT_FAILURE);
	}
	int temp_position = this->position - 1;
	char referenceBase = 'N';

	if (!fasta_file.GetBase(this->RefId, temp_position, referenceBase)) {
		throw runtime_error("Unable to get reference base from FASTA file.");
	}
	referenceBase = toupper(referenceBase);
	fasta_file.Close();

	if(!is_valid_ref(referenceBase)) {
		string str_base(1, referenceBase);
		string error_message = "Invalid reference base (" + str_base + ") found at " +  this->chrom + " : " + intToString(this->position);
		throw runtime_error(error_message);
	}
	return referenceBase;
}

void Locus::fill(string ref_fasta, string chrom, int position) {
	this->chrom = chrom;

	/* This is STRICTLY the fasta refId , which is NOT necessarily the same as the BAM one */
	this->RefId = getFastaRefId(ref_fasta, chrom);

	/* Interface is 1-based, bamtools is 0-based */
	this->position = position + 1;
	this->ref = getRefFromFasta(ref_fasta);
}


int getFastaRefId(string ref_fasta, string chrom) {
	FastaE fasta_file;
		if(!fasta_file.Open(ref_fasta, ref_fasta + ".fai")) {
			throw runtime_error("Unable to open fasta file and/or fasta index");
		}

	vector <string> refNames = fasta_file.GetReferenceNames();
    int refId = distance(refNames.begin(), find(refNames.begin(), refNames.end(), chrom));

//	cout << "I will retrieve data at " << refNames.at(refId) << " for BAM refID " << refId << " which is chrom " << chrom << endl;
//	vector <int> refLengths = fasta_file.GetReferenceLengths() ;
//	for(int i =0 ; i < refLengths.size();i++) {
//		cout << "in ref " << refLengths.at(i) << endl;
//	}

	fasta_file.Close();
	 if ( refId == refNames.size() ) return -1;
	    else return refId;

}
int Locus::get_longest_homopolymer_run(string ref_fasta, int threshold, string alt_allele) const  {
	int current_position = this->position - 1;
	assert(alt_allele.size() == 1 );
	char candidate_base = alt_allele[0];
	FastaE fasta_file;
	if(!fasta_file.Open(ref_fasta, ref_fasta + ".fai")) {
		throw runtime_error("Unable to open fasta file and/or fasta index");
	}

	char next_base = 'N';
	if (!fasta_file.GetBase(this->RefId, current_position + 1, next_base)) {
		throw runtime_error("unable to get reference base ");
	}
	next_base = toupper(next_base);

	char previous_base = 'N';
	if (!fasta_file.GetBase(this->RefId, current_position - 1, previous_base)) {
		throw runtime_error("Unable to get reference base ");
	}
	previous_base = toupper(previous_base);

	/* Scenarios */
	int total_count = 0;
	int count_forward = 1;
	int count_backward = 1;

	/* If the next base matches the candidate, count how many times it is repeated*/
	if (candidate_base == next_base) {
		char temp_base = next_base;
		int i = 0;
		while(temp_base == next_base && i < threshold - 1 ) {
			if (!fasta_file.GetBase(this->RefId, current_position + 2 + i, temp_base)) {
				throw runtime_error("unable to get reference base ");
			}
			temp_base = toupper(temp_base);
			if(temp_base == next_base) {
				count_forward++;
			}
			i++;
		}
	}

	if (candidate_base == previous_base) {
		char temp_base = previous_base;
		int i = 0;
		while(temp_base == previous_base && i < threshold - 1 ) {

			if (!fasta_file.GetBase(this->RefId, current_position - 2 - i, temp_base)) {
				throw runtime_error("unable to get reference base ");
			}
			temp_base = toupper(temp_base);
			if(temp_base == previous_base) {
				count_backward++;
			}
			i++;
		}

	}

//	if (next_base == previous_base && next_base == candidate_base ) {
//		total_count = count_forward + count_backward; /* means site is within a homopolymer! */
//		if(total_count >= threshold) {
//			return true;
//		}
//	}

	fasta_file.Close();
	return max(count_forward, count_backward);
}

bool Locus::is_homopolymer(string ref_fasta, int threshold, string alt_allele) const {
	int count = get_longest_homopolymer_run(ref_fasta, threshold, alt_allele);
	return count >= threshold;
}

bool is_valid_ref(char base) {
	switch (base) {
		case 'A':
		case 'C':
		case 'G':
		case 'T':
		case 'N':
			return true;
		default:
			return false;
	}
}
