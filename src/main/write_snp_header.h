/*
 * write_snp_header.h
 *
 *  Created on: Sep 20, 2014
 *      Author: malvinajosephidou
 */

#ifndef WRITE_SNP_HEADER_H_
#define WRITE_SNP_HEADER_H_

#include "Settings.h"
#include "GibbsSettings.h"
void write_VCF_header(string filename, FilterSettings filter_settings,
		ProgramSettings program_settings, GibbsSettings gibbs_settings, int argc, char* argv[]);
void write_features_file_header(string filename, int NumberOfSamples);
#endif /* WRITE_SNP_HEADER_H_ */
