/*
 * Results.cpp
 *
 *  Created on: Feb 26, 2015
 *      Author: joseph07
 */

#include "Results.h"
#include "Data.h"
#include "SummaryResults.h"
#include "utils.h"
#include <assert.h>
#include <iomanip>

void Results::process(const MultisampleData &ms_data,
		const GibbsSampler &inference_output, const GibbsSettings &gibbs_settings,
		const FilterSettings & filter_settings, const ProgramSettings &program_settings) {
	/* TODO: What if we aren't doing convergence tests? */
	failed_to_converge = is_failed_to_converge(inference_output.executedGibbsIters, gibbs_settings.maxIter);
	SummaryResults::getInstance()->updateSummary(event_type, failed_to_converge);
}

string Results::get_all_allelic_compositions() const {
	string allelic_comp_distr = "|" + normal_sample.get_MAP_state().get_as_str() + "|";
	for(int i = 0; i < tumour_samples.size(); i++) {
		allelic_comp_distr = allelic_comp_distr + tumour_samples.at(i).get_MAP_state().get_as_str() + "|";
	}
	return allelic_comp_distr;
}

void ReportedResults::set_variant_quality(const map <string, int > &countsPerAggregateState ) {
	    vector <int > countsPerStateVec;
		for (map <string,int >::const_iterator it = countsPerAggregateState.begin(); it
				 != countsPerAggregateState.end(); ++it) {
					countsPerStateVec.push_back(it->second);
			}
		qual_control.variant_quality = compQual(countsPerStateVec);
}

void ReportedResults::process(const MultisampleData &ms_data,
		const GibbsSampler &inference_output, const GibbsSettings &gibbs_settings,
		const FilterSettings & filter_settings, const ProgramSettings &program_settings) {
	Results::process(ms_data, inference_output, gibbs_settings, filter_settings, program_settings);
	set_variant_quality(inference_output.countsPerAggregateState);
	apply_filters(ms_data, filter_settings, program_settings.ref_fasta);
	set_filter_flag(filter_settings);
	appendVCF(program_settings.filename_output , ms_data);
	print_result(ms_data.data, ms_data.pos, inference_output, program_settings.print);
}

void ReportedResults::apply_filters(const MultisampleData &ms_data, const FilterSettings &filter_settings, string fasta) {
	ReportedResults::apply_filters(ms_data);
	return;
}

void ReportedResults::apply_filters(const MultisampleData &ms_data) {
	qual_control.mean_depth =   double(ms_data.total_depth) / ms_data.data.size();
	qual_control.fractionFailingQC = double( ms_data.readsFailingQC) / (ms_data.total_depth + ms_data.readsFailingQC);
	qual_control.depth_in_normal = ms_data.data.at(0).depth;
	qual_control.isTriallelic = findStateWithMostAlleles().size() > 2;
	return;
}

void ReportedResultsValidVariant::apply_filters(const MultisampleData &ms_data, const FilterSettings &filter_settings, string fasta) {
	ReportedResults::apply_filters(ms_data);
	qual_control.strand_bias = get_strand_bias(ms_data.data, filter_settings.SB_threshold);
	qual_control.isNormalTriallelic= is_normal_triallelic(filter_settings.minVariantReadsForTriallelicSite, ms_data.data.at(0).counts.total);

	return;
}

void ReportedResultsSomatic::apply_filters(const MultisampleData &ms_data, const FilterSettings &filter_settings, string fasta) {
	ReportedResultsValidVariant::apply_filters(ms_data, filter_settings, fasta);
	qual_control.startEndDist = getStartEndReadDistances(ms_data.data);
	qual_control.areReadsClustered = are_reads_clustered(qual_control.startEndDist);
	qual_control.longestHomopolymerRun = get_longest_homopolymer_run(ms_data.pos, fasta, filter_settings.homopolLengthThreshold);
	qual_control.isHomopolymer = (qual_control.longestHomopolymerRun >= filter_settings.homopolLengthThreshold);
	qual_control.isEvidenceWeak = is_evidence_weak(ms_data.data, filter_settings.weakEvidenceThreshold);
	qual_control.variant_freqs = compute_variant_freqs(ms_data.data);
	return;
}

void ReportedResultsSomatic::extract_features(const MultisampleData &ms_data, const GibbsSampler &inference_output,
		string features_file) {

	if(features_file != "") {
		ofstream arff;
		arff.open(features_file.c_str(), ios_base::out | ios_base::app);
		arff    << ms_data.pos.chrom 							<< "," << ms_data.pos.position 							<< ","
				<< this->qual_control.normalContaminationCounts << "," << this->qual_control.depth_in_normal    		<< ","
				<< this->qual_control.mean_depth 				<< "," << this->qual_control.fractionFailingQC  		<< ","
				<< this->qual_control.areReadsClustered			<< ","
				<< this->qual_control.longestHomopolymerRun		<< "," << this->qual_control.strand_bias 				<< ","
				<< this->qual_control.variant_quality   		<< "," << this->failed_to_converge					    << ","
				<< this->qual_control.startEndDist["start"].getMedian() << "," <<  this->qual_control.startEndDist["start"].getMedianAbsoluteDeviation()  << ","
				<< this->qual_control.startEndDist["end"].getMedian() 	<< "," <<  this->qual_control.startEndDist["end"].getMedianAbsoluteDeviation();

				for(int i=0; i< this->qual_control.variant_freqs.size();i++) {
					arff << "," << this->qual_control.variant_freqs.at(i);
				}
			  arff << endl;
		arff.close();
	  }
}



void ReportedResultsSNV::apply_filters(const MultisampleData &ms_data, const FilterSettings &filter_settings, string fasta) {
	ReportedResultsSomatic::apply_filters(ms_data, filter_settings, fasta);
	set_is_normal_contaminated(ms_data.data.at(0), filter_settings.normalContaminationThreshold);
	return;
}

void ReportedResultsLOH::apply_filters(const MultisampleData &ms_data, const FilterSettings &filter_settings, string fasta) {
	ReportedResultsSomatic::apply_filters(ms_data, filter_settings, fasta);
	return;
}

void ReportedResultsGermline::apply_filters(const MultisampleData &ms_data, const FilterSettings &filter_settings, string fasta) {
	ReportedResultsValidVariant::apply_filters(ms_data, filter_settings, fasta);
	qual_control.variant_freqs = compute_variant_freqs(ms_data.data);
	return;
}

void ReportedResultsWildtype::apply_filters(const MultisampleData &ms_data, const FilterSettings &filter_settings, string fasta) {
	ReportedResults::apply_filters(ms_data);
	return;
}

void ReportedResultsUnexpectedEvent::apply_filters(const MultisampleData &ms_data, const FilterSettings &filter_settings, string fasta) {
	ReportedResults::apply_filters(ms_data);
	return;
}

void ReportedResults::set_filter_flag(const FilterSettings& filter_settings) {
		this->qual_control.filter_flags= "";

		if(this->qual_control.variant_quality < filter_settings.flag_low_SNV_qual ) {
			this->qual_control.filter_flags += (qual_control.filter_flags.length () == 0 ? "LOW_QUAL" : ";LOW_QUAL");
		}

		if(this->qual_control.fractionFailingQC > filter_settings.fraction_failing_QC ) { /* TODO: Revisit this setting */
			this->qual_control.filter_flags += (qual_control.filter_flags.length () == 0 ? "BAD_READS" : ";BAD_READS");
		}

		if(this->qual_control.mean_depth < filter_settings.flag_low_depth) {
			this->qual_control.filter_flags += (qual_control.filter_flags.length () == 0 ? "LOW_AVERAGE_DEPTH" : ";LOW_AVERAGE_DEPTH");
		}

		if(this->qual_control.depth_in_normal < filter_settings.flag_low_depth) {
			this->qual_control.filter_flags += (qual_control.filter_flags.length () == 0 ? "LOW_DEPTH_IN_NORMAL" : ";LOW_DEPTH_IN_NORMAL");
		}

		if(this->qual_control.isNormalTriallelic ) {
			this->qual_control.filter_flags += (qual_control.filter_flags.length () == 0 ? "LOW_QUAL_NORMAL" : ";LOW_QUAL_NORMAL");
		}

		if(this->event_type == UNEXPECTED) {
			this->qual_control.filter_flags += (qual_control.filter_flags.length () == 0 ? "EXCESSIVE_MUTS" : ";EXCESSIVE_MUTS");
		}

		if(this->failed_to_converge) {
			this->qual_control.filter_flags += (qual_control.filter_flags.length () == 0 ? "MAX_ITER" : ";MAX_ITER");
		}

		/* -1 is the default value : if we get this , computation was not performed so don't flag */
		if(this->qual_control.strand_bias < filter_settings.SB_threshold && this->qual_control.strand_bias != STRAND_BIAS_DEFAULT ) {
			this->qual_control.filter_flags += (qual_control.filter_flags.length () == 0 ? "STRAND_BIAS" : ";STRAND_BIAS");
		}

		if(this->qual_control.areReadsClustered) {
			this->qual_control.filter_flags += (qual_control.filter_flags.length () == 0 ? "CLUSTERED_READS" : ";CLUSTERED_READS");
		}

		if(this->qual_control.isEvidenceWeak) {
			this->qual_control.filter_flags += (qual_control.filter_flags.length () == 0 ? "WEAKLY_SUPPORTED_VARIANT" : ";WEAKLY_SUPPORTED_VARIANT");
		}

		if(this->qual_control.isNormalContaminated) {
			this->qual_control.filter_flags += (qual_control.filter_flags.length () == 0 ? "NORMAL_CONTAMINATION" : ";NORMAL_CONTAMINATION");
		}

		if(this->qual_control.isHomopolymer && this->qual_control.normalContaminationFreq > 0.01 ) {
			this->qual_control.filter_flags += (qual_control.filter_flags.length () == 0 ? "HOMOPOLYMER" : ";HOMOPOLYMER");
		}

		if(qual_control.filter_flags.length() == 0) {
			this->qual_control.filter_flags = "PASS" ;
		}

		return;
}

string ReportedResults::findNonUbiqAlleles() {
	string nonUbiqAlleles = "";
	map < char, int > alleleFrequency;
	string state = normal_sample.get_MAP_state().get_as_str();

	for(int j = 0 ; j < state.size(); j++) {
		alleleFrequency[state.at(j)]++;
	}

	for(int i = 0; i <  tumour_samples.size() ; i++) {
		state = tumour_samples.at(i).get_MAP_state().get_as_str();
		for(int j = 0 ; j < state.size(); j++) {
			alleleFrequency[state.at(j)]++;
		}
	}

	for(map <char, int>::iterator mapIter = alleleFrequency.begin(); mapIter != alleleFrequency.end(); ++mapIter) {
		if(mapIter->second < tumour_samples.size() + 1) {
			nonUbiqAlleles = nonUbiqAlleles + mapIter->first;
		}
	}
	return nonUbiqAlleles;
}

/* TODO: Might not want to repeat computing this , compute once and save? */
string ReportedResults::findStateWithMostAlleles() const {
	string stateWithMostAlleles = normal_sample.get_MAP_state().get_as_str();
	for ( int i = 0; i < tumour_samples.size() ; i++) {
		string tumour_state = tumour_samples.at(i).get_MAP_state().get_as_str();
		if( tumour_state.size() > stateWithMostAlleles.size()) {
			stateWithMostAlleles = tumour_state;
		}
	}
	return stateWithMostAlleles;
}

Counts pool_counts_of_mutated_samples(const SampleResults &normal_sample, const vector <SampleResults> &tumour_samples,
		string mutated_state, const vector <Data> &data) {

	Counts counts_in_mutated_samples;

	if(normal_sample.get_MAP_state().get_as_str() == mutated_state) {
		counts_in_mutated_samples = counts_in_mutated_samples + data.at(0).counts;
		}

	for(int j = 0; j < tumour_samples.size(); j++) {
		if(tumour_samples.at(j).get_MAP_state().get_as_str() == mutated_state) {
			counts_in_mutated_samples = counts_in_mutated_samples + data.at(j+1).counts;
		}
	}
	return counts_in_mutated_samples;
}

void ReportedResultsSNV::set_is_normal_contaminated(const Data& normal_data, double threshold) {
	if(event_type == UNEXPECTED) {
		qual_control.isNormalContaminated = false;
		return;
	}
	qual_control.isNormalContaminated = is_normal_contaminated(normal_data, threshold);
	return;
}

void ReportedResultsSNV::set_normal_contamination(const Data& normal_data) {
	const string nonUbiqAlleles = findNonUbiqAlleles();
	assert (nonUbiqAlleles.size() == 1);
	double variant_freq = 0;
	/* If normal sample isn't supposed to have the variant */
	if(normal_sample.get_MAP_state().get_as_str().find_first_of(nonUbiqAlleles) == string::npos) {
		qual_control.normalContaminationCounts =  normal_data.counts.total.at(base2ind(nonUbiqAlleles));
		qual_control.normalContaminationFreq = (double) qual_control.normalContaminationCounts  / normal_data.depth;
	}
	return;
}

bool ReportedResultsSNV::is_normal_contaminated(const Data& normal_data, double threshold) {
	set_normal_contamination(normal_data);
	if(qual_control.normalContaminationFreq > threshold && qual_control.normalContaminationCounts > 1) {
		return true;
	}

	else
		return false;
}

//bool ReportedResultsSomatic::is_homopolymer(const Locus &pos, string fasta_file, int homopol_threshold) {
//	/* Do not flag germline events for homopolymer runs */
//	assert(event_type != GERMLINE);
//	string candidate_alleles;
//	candidate_alleles = findNonUbiqAlleles();
//	return pos.is_homopolymer(fasta_file, homopol_threshold, candidate_alleles);
//}

//void ReportedResultsSomatic::set_is_homopolymer(const Locus &pos, string fasta_file, int homopol_threshold) {
//	qual_control.longestHomopolymerRun = this->get_longest_homopolymer_run(pos, fasta_file, homopol_threshold);
//	qual_control.isHomopolymer = (qual_control.longestHomopolymerRun >= homopol_threshold);
//}

int ReportedResultsSomatic::get_longest_homopolymer_run(const Locus &pos, string fasta_file, int homopol_threshold) {
	assert(event_type != GERMLINE);
	string candidate_alleles;
	candidate_alleles = findNonUbiqAlleles();
	return pos.get_longest_homopolymer_run(fasta_file, homopol_threshold, candidate_alleles);
}

double ReportedResultsValidVariant::get_strand_bias(const vector<Data>& data,
										   const double &SB_threshold) {

	double strand_bias = -1;
	assert(event_type != UNEXPECTED);
	/* 	If we have inferred something unexpected like A,AC,AT,AC : it will just
	 *  randomly pick one of the two and try to compute SB. The previous check ensures we never run the test
	 *  on such "unexpected sites*/
	string maxMAP = findStateWithMostAlleles();

	Counts counts_in_mutated_samples = pool_counts_of_mutated_samples(normal_sample, tumour_samples, maxMAP, data);
	vector <unsigned int>  forward_counts_in_mutated_samples = counts_in_mutated_samples.get_forward();

	if(maxMAP.size() == 2) {
		strand_bias = fisher_test(forward_counts_in_mutated_samples.at(base2ind(maxMAP[0])),
										forward_counts_in_mutated_samples.at(base2ind(maxMAP[1])),
										counts_in_mutated_samples.reverse.at(base2ind(maxMAP[0])),
										counts_in_mutated_samples.reverse.at(base2ind(maxMAP[1]))
										);
	}

	/* TODO: check we aren't doing this twice */
	if(strand_bias <  SB_threshold && strand_bias != -1 ) {
		SummaryResults::getInstance()->n_strand_bias++;
	}
	return strand_bias;
}

double ReportedResultsSomatic::compute_max_variant_freq(const vector<Data> &data) {
	const string nonUbiqAlleles = findNonUbiqAlleles();
	assert (nonUbiqAlleles.size() == 1);
	double variant_freq = 0;
	double temp = 0;
	if(normal_sample.get_MAP_state().get_as_str().find_first_of(nonUbiqAlleles) != string::npos) {
		temp = (double) data.at(0).counts.total.at(base2ind(nonUbiqAlleles)) / data.at(0).depth;
		variant_freq = (variant_freq > temp) ? variant_freq : temp;
	}

	for(int j = 0 ; j < tumour_samples.size() ; j++) {
			if(tumour_samples.at(j).get_MAP_state().get_as_str().find_first_of(nonUbiqAlleles) != string::npos) {
				temp = (double) data.at(j+1).counts.total.at(base2ind(nonUbiqAlleles)) / data.at(j+1).depth;
				variant_freq = (variant_freq > temp) ? variant_freq : temp;
			}
	}
	return variant_freq;
}

/* Assumes there is a somatic event */
vector <double> ReportedResultsSomatic::compute_variant_freqs(const vector<Data> &data) {
	const string nonUbiqAlleles = findNonUbiqAlleles();
	assert (nonUbiqAlleles.size() == 1);
	double freq = 0;
	vector <double> freqs;
	freq = (double) data.at(0).counts.total.at(base2ind(nonUbiqAlleles)) / data.at(0).depth;
	freqs.push_back(freq);
	for(int j = 0 ; j < tumour_samples.size() ; j++) {
		freq = (double) data.at(j+1).counts.total.at(base2ind(nonUbiqAlleles)) / data.at(j+1).depth;
		freqs.push_back(freq);
	}
	return freqs;
}

/* TODO: CHECK THIS!: For germline one computer variant allele frequency using the LAST alternate allele as the variant */
vector <double> ReportedResultsGermline::compute_variant_freqs(const vector<Data> &data) {
	string variant_allele = alternate_alleles.substr( alternate_alleles.size() -1 , 1);
	double freq = 0;
	vector <double> freqs;
	freq = (double) data.at(0).counts.total.at(base2ind(variant_allele)) / data.at(0).depth;
	freqs.push_back(freq);
	for(int j = 0 ; j < tumour_samples.size() ; j++) {
		freq = (double) data.at(j+1).counts.total.at(base2ind(variant_allele)) / data.at(j+1).depth;
		freqs.push_back(freq);
	}
	return freqs;
}

bool ReportedResultsSomatic::is_evidence_weak(const vector <Data> &data, double weakEvidenceThreshold) {
	assert (event_type == LOH || event_type == SOMATIC );
	return compute_max_variant_freq(data) < weakEvidenceThreshold;
}

map <string, ReadDistance> ReportedResultsSomatic::getStartEndReadDistances(const vector <Data>& data) {
	assert (event_type == LOH || event_type == SOMATIC );
		const string nonUbiqAlleles = findNonUbiqAlleles();
		assert (nonUbiqAlleles.size() == 1);

		vector <int> variant_reads_start;
		vector <int> variant_reads_end;

		if(normal_sample.get_MAP_state().get_as_str().find_first_of(nonUbiqAlleles) != string::npos) {
			for(int i = 0; i < data.at(0).reads.size(); i++) {
				Read candidate_read = data.at(0).reads.at(i);
				if(candidate_read.base == base2ind(nonUbiqAlleles)) {
					variant_reads_start.push_back(candidate_read.dist_start);
					variant_reads_end.push_back(candidate_read.dist_end);
				}
			}
		}
		for(int j = 0 ; j < tumour_samples.size() ; j++) {
			if(tumour_samples.at(j).get_MAP_state().get_as_str().find_first_of(nonUbiqAlleles) != string::npos) {
				for(int i = 0; i < data.at(j+1).reads.size(); i++) {
					Read candidate_read = data.at(j+1).reads.at(i);
					if(candidate_read.base == base2ind(nonUbiqAlleles)) {
						variant_reads_start.push_back(candidate_read.dist_start);
						variant_reads_end.push_back(candidate_read.dist_end);
					}
				}
			}
		}

		ReadDistance from_start(variant_reads_start);
		ReadDistance from_end(variant_reads_end);

		map <string, ReadDistance> dist;
		dist["start"] = from_start;
		dist["end"] = from_end;
		return dist;
}

bool ReportedResultsSomatic::are_reads_clustered(map <string, ReadDistance> &dist) {
	if(dist["start"].getMedian() <= 10 || dist["end"].getMedian() <= 10) {
		if(dist["start"].getMedianAbsoluteDeviation() <= 3 || dist["end"].getMedianAbsoluteDeviation() <= 3 ) {
			return true;
		}
	}
	return false;
}

void ReportedResults::appendVCF(string VCF, const MultisampleData &ms_data ) const {
	string format = qual_control.isTriallelic ?  "A:GQ:SS:BCOUNT:DP" : "GT:A:GQ:SS:BCOUNT:DP";
	ofstream snps;
	snps.open(VCF.c_str(), ios_base::out | ios_base::app);
	snps << setprecision(4); snps << fixed; snps << left;

	/* TODO: If strand bias test hasn't been done, don't print strand bias */
	snps 	 << ms_data.pos.chrom      		  << '\t'
			 << ms_data.pos.position 		  << '\t'
			 << "." 						  << '\t'
			 << ms_data.pos.ref   			  << '\t'
			 << add_delimiter_to_string(alternate_alleles, ",")  << '\t'
			 << qual_control.variant_quality  					 << '\t'
			 << qual_control.filter_flags  						 << '\t'
			 << "NS=" 	   										 << ms_data.data.size()
			 << ";DISTR="  										 << this->get_all_allelic_compositions();

	if(qual_control.strand_bias != STRAND_BIAS_DEFAULT) {
		snps <<  ";SB="  << qual_control.strand_bias;
	}

	snps     << '\t'  << format << '\t';

	if(!qual_control.isTriallelic) {
		snps << this->normal_genotype  << ":";
	}

	snps    << this->normal_sample.get_MAP_state().get_as_str()
		    << ":"  << this->normal_sample.get_MAP_state().getQual()
		    << ":"  << this->normal_sample.get_somatic_status()
		    << ":"  << ms_data.data.at(0).counts.total.at(0)
		    << "," 	<< ms_data.data.at(0).counts.total.at(1)
		    << ","  << ms_data.data.at(0).counts.total.at(2)
		    << ","  << ms_data.data.at(0).counts.total.at(3)
		    << ":" 	<< ms_data.data.at(0).depth << '\t' ;

	for(size_t i = 0; i < tumour_samples.size(); i++) {
		if(!qual_control.isTriallelic) {
			snps << this->tumour_genotypes.at(i)	 << ":";
		}

		snps << this->tumour_samples.at(i).get_MAP_state().get_as_str()
			 << ":" << this->tumour_samples.at(i).get_MAP_state().getQual()
			 << ":" << this->tumour_samples.at(i).get_somatic_status()
			 << ":" << ms_data.data.at(i+1).counts.total.at(0)
			 << "," << ms_data.data.at(i+1).counts.total.at(1)
			 << "," << ms_data.data.at(i+1).counts.total.at(2)
			 << "," << ms_data.data.at(i+1).counts.total.at(3)
			 << ":" << ms_data.data.at(i+1).depth ;

		if(i < tumour_samples.size()-1) {
			snps << '\t';
		}
	}
	snps << endl;
	snps.close();
	return;
}

//void ReportedResults::print_result(const vector<Data>& data, const Locus &locus,
//		const GibbsSampler &inference_output, bool doPrint ) const {
//
//	if(doPrint) {
//		cout << SummaryResults::event_types.at(event_type)  << "event, number " << SummaryResults::getInstance()->n_event_type.at(event_type)
//				 << ",found at chromosome " << locus.chrom << " at " << locus.position << endl;
//		cout << "Ref is: " << locus.ref << endl;
//		cout << "" << endl;
//		cout << "   A, C, G, T" << endl;
//		cout << "BL ";
//		copy(data.at(0).counts.total.begin(), data.at(0).counts.total.end(),
//				ostream_iterator<int>(cout, " "));
//		cout << "" << endl;
//
//		for(int i = 1; i < data.size(); i++) {
//			cout << "T"<< i << " " ;
//			copy(data.at(i).counts.total.begin(),
//					data.at(i).counts.total.end(), ostream_iterator<int>(cout, " "));
//			cout << "" << endl;
//		}
//
//		cout << "" << endl;
//		cout << "Possible allelic compositions: " << endl;
//
//		for (map <string,int >::const_iterator it = inference_output.countsPerAggregateState.begin(); it
//		!= inference_output.countsPerAggregateState.end(); ++it) {
//			cout << it->first << " " << it->second << endl;
//		}
//
//		cout << "" << endl;
//
//		cout << "MAP estimate per sample " << this->get_all_allelic_compositions() << endl;
//		cout << "SNV quality per sample is |";
//		cout << this->normal_sample.get_MAP_state().getQual() << "|";
//		for(int i = 0; i < tumour_samples.size(); i++) {
//			cout << this->tumour_samples.at(i).get_MAP_state().getQual() << "|";
//		}
//
//		cout << endl;
//		cout << "Strand bias is " << this->qual_control.strand_bias << endl;
//		cout << endl;
//	}
//}

void ReportedResults::print_result(const vector<Data>& data, const Locus &locus,
		const GibbsSampler &inference_output, bool doPrint ) const {

	if(doPrint) {
		cout << endl;
		cout << "----------------------------------------------------------------------------------------------------------------" << endl;
		cout <<  SummaryResults::event_types.at(event_type)  << '\t' << "#" << SummaryResults::getInstance()->n_event_type.at(event_type) <<  endl;
		cout << endl;
		cout << "CHROM:" << locus.chrom   << '\t' <<  "POS:" << locus.position  << '\t' <<  "REF:" << locus.ref <<  '\t'
			 << "VARIANT CALL:" << this->get_all_allelic_compositions() << '\t'
			 <<  "FLAGS:" << qual_control.filter_flags << '\t' <<  "SB:" << qual_control.strand_bias  << endl;

		cout << "" << endl;
		cout << "Observed reads per sample: " << endl;
		cout << endl;
		cout << "   A, C, G, T" << endl;
		cout << "BL ";
		copy(data.at(0).counts.total.begin(), data.at(0).counts.total.end(),
				ostream_iterator<int>(cout, " "));
		cout << "" << endl;

		for(int i = 1; i < data.size(); i++) {
			cout << "T"<< i << " " ;
			copy(data.at(i).counts.total.begin(),
					data.at(i).counts.total.end(), ostream_iterator<int>(cout, " "));
			cout << "" << endl;
		}

		cout << "" << endl;
		cout << "Gibbs samples: " << endl;

		for (map <string,int >::const_iterator it = inference_output.countsPerAggregateState.begin(); it
		!= inference_output.countsPerAggregateState.end(); ++it) {
			cout << it->first << " " << it->second << endl;
		}

		cout << "" << endl;


	}
}



void ReportedResults::set_genotypes() {
		normal_genotype = normal_sample.get_genotype(ref, alternate_alleles);
		for(int i = 0; i < tumour_samples.size(); i++) {
			tumour_genotypes.push_back(tumour_samples.at(i).get_genotype(ref, alternate_alleles));
		}
		return;
	}
void ReportedResults::set_alternate_alleles(char ref) {
		alternate_alleles = "";
			for(int i = 0; i < tumour_samples.size(); i++) {
				string tumour_state = tumour_samples.at(i).get_MAP_state().get_as_str();
				alternate_alleles = get_more_alternate_alleles(alternate_alleles, tumour_state, ref);
			}
			alternate_alleles = get_more_alternate_alleles(
					alternate_alleles, normal_sample.get_MAP_state().get_as_str(), ref);
			return;
	}

string ReportedResults::get_alternare_alleles() const {
	return alternate_alleles;
}

string ReportedResults::get_more_alternate_alleles( string already_found_alleles, string state, char ref) const {
		for(int j = 0; j < state.size(); j++) {
				char t_allele = state[j];
				if(already_found_alleles.find(t_allele) == string::npos && t_allele != ref) {
					already_found_alleles = already_found_alleles + t_allele;
				}
			}
		return already_found_alleles;
	}

bool is_failed_to_converge(int executed_cycles, int maxIter) {
	if(executed_cycles >= maxIter)
		return true;
	else
		return false;
}



