
//
//  GibbsAnalysis.h
//
//
//  Created by Malvina Josephidou on 06/02/2013.
//
//

#ifndef ____GibbsAnalysis__
#define ____GibbsAnalysis__
#include <iostream>
#include <string>
#include <vector>
#include<fstream>
#include<istream>
#include<sstream>
#include <cmath>
#include <stdio.h> 
#include <vector>
#include <algorithm> 
#include <iterator> 
#include <stdlib.h>
#include <map>
#include <utility>
#include <cstring>
#include <iterator>
#include <map>
#include <boost/ptr_container/ptr_vector.hpp>

#include "GibbsSettings.h"
#include "Gibbs.h"
#include "ConditionalDistr.h"
#include "NormalSample.h"
#include "MultisampleData.h"
#include "SampleResults.h"
using namespace std;

variant_type_t get_event_type(const vector <SampleResults> &unique_tumour_samples, const SampleResults &normal_sample );
vector <SampleResults> get_mutated_tumour_states(const vector <SampleResults> &tumour_samples, const SampleResults &normal_sample);

class GibbsSampler {
public:
    vector<Gibbs*> samplerHistory;
    map<string,int> countsPerAggregateState;
    int executedGibbsIters;
	int numberOfParameters;

    GibbsSampler(int N, int initState) {
		numberOfParameters = N;
		executedGibbsIters = 0;
		for(int i = 0; i < N; i++) {
			samplerHistory.push_back(new Gibbs);
			//int p = ref2state(ref);
			//int p = rand() % (9 + 0);
			//p = NormalSample::get_normal_states().at(p).getInteger(); // Use random number to sample from normal state as initialization.
			samplerHistory.at(i)->samples.push_back(initState);
		}
	}
    
    ~GibbsSampler() {
        while(!samplerHistory.empty()) {
            delete samplerHistory.back(), samplerHistory.pop_back();
        }
    }

    void drawSamples(const GibbsSettings &gibbs_settings, boost::ptr_vector<ConditionalDistr> &conditional_distr );
    bool convergenceCheck();
    void computeCounts(const double &burnin, const int &thin);
};

GibbsSampler runGibbsSampler(const GibbsSettings &gibbs_settings,
		const ProgramSettings &program_settings, const MultisampleData * ms_data);

#endif /* defined(____GibbsAnalysis__) */
