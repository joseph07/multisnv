/*
 * MAPstate.cpp
 *
 *  Created on: Feb 24, 2015
 *      Author: joseph07
 */

#include "MAPstate.h"
#include "utils.h"
using namespace std;

MAPstate::~MAPstate() {
	// TODO Auto-generated destructor stub
}

void MAPstate::init(vector <int> counts_per_state) {
	as_int =  findMostFrequentElement(counts_per_state);
	as_str =  intState2string(as_int);
	qual   =  compQual(counts_per_state);
}

int MAPstate::get_as_int() const {
	return as_int;
}

string MAPstate::get_as_str() const {
	return as_str;
}

long double MAPstate::getQual() const {
	return qual;
}
