/*
 *  sim_utils.cpp
 *  multisnv_proj
 *
 *  Created by Malvina Josephidou on 31/08/2014.
 *  Copyright 2014 University of Cambridge. All rights reserved.
 *
 */
#include<iomanip>

#include "sim_utils.h"
#include "SimCollection.h"
#include "SimulationSettings.h"


void run_multisnv_sim(const ProgramSettings& program_settings, const GibbsSettings & gibbs_settings,
		const SimulationSettings& sim_settings) {

    SummaryResults* ptr_summary = SummaryResults::getInstance();

	/* Simulate events */
    SimCollection* ptr_sim = new SimCollection(program_settings.NumberOfSamples);
	ptr_sim->pos.chrom = "chr1";
	ptr_sim->pos.ref = 'A';

	for(int i = 0; i < sim_settings.sims; i++) {
	    ptr_sim->SimulateData(sim_settings);
		stringstream ss;
		ss << i+1;
		//ptr_sim->pos.sequenceID = ss.str(); /* integer to string TODO: use pos.position*/
		ptr_sim->rungibbs( gibbs_settings, program_settings);
		ptr_summary->n_runs++;
	}
	delete ptr_sim;
}

int AddNoise (int allele, double pError) {
	int index;
	long double Rint = rand () / double(RAND_MAX);
	
	if (Rint  > pError) {
		index = allele;
	}
	
	else {
		vector <int> wrongAlleles;
		for (int i = 0; i < 4; i++) {
			if( i != allele){
				wrongAlleles.push_back(i);  /* variant alleles */
			}			
		}
		
		double low = 0;
		double high = pError /3;
		index = wrongAlleles.at(0);
		
		for (int i = 0 ; Rint > low && Rint > high; i++) {		
			low  = high;
			high = high + (pError /3) ;
			index = wrongAlleles.at(i+1);
		}			
	}
	return index;
}

void write_vcf_sim_header(const FilterSettings& filter_settings,
		const ProgramSettings& program_settings, const GibbsSettings& gibbs_settings,
		const SimulationSettings& sim_settings) {

	ofstream snps;
	snps.open(program_settings.filename_output.c_str(), ios_base::out | ios_base::app);
    snps << setprecision(7); snps << fixed; snps << left;

    snps << "##fileformat = VCFv4.0" 								  << endl;
    snps << "##source = multiSNV" 									  << endl;
    snps << "##reference = hg19" 									  << endl;
    snps << "##phasing = none" 										  << endl;

    snps << "##Simulated reads!" << endl;
    snps << "##Simulated normal impurity=" << sim_settings.impurity << endl;
    snps << "##Simulated variant allele frequency=" << sim_settings.MAF << endl;
    snps << "##Number of simulations : " << sim_settings.sims << endl;
    snps << "##Simulations error : " << sim_settings.pError << endl;
    snps << "##Simulated depth of coverage: " << sim_settings.depth << endl;

	snps << "##Seed used = "										  << program_settings.seed 				   << endl;
    snps << "##Number of samples = "                                  << program_settings.NumberOfSamples 	   << endl;
    snps << "##Minimun number of variant alleles = "                  << filter_settings.minVarAlleles		   << endl;
    snps << "##Maximum number of mismatch reads allowed in normal = " << filter_settings.MaxVarNorm 		   << endl;
    snps << "##Minimum average depth required = "                     << filter_settings.min_depth			   << endl;
    snps << "##Median tumour coverage = "                             << gibbs_settings.MedianTumourCoverage   << endl;
    snps << "##Median normal coverage = "                             << gibbs_settings.MedianNormalCoverage   << endl;
    snps << "##Minimum Base Quality = " 							  << filter_settings.ReadQualThreshold	   << endl;
    snps << "##Minimum Mapping Quality = " 							  << filter_settings.MappingQualThreshold  << endl;
	snps << "##Mutation Rate = " 									  << gibbs_settings.mutrate				   << endl;
    snps << "##Number of Gibbs iterations = "						  << gibbs_settings.gibbs_runs 			   << endl;
    snps << "##Thinning parameter = " 								  << gibbs_settings.thin 				   << endl;
    snps << "##Burn in fraction = "  						          << gibbs_settings.thin 				   << endl;
    snps << "##Run convergence check = " 							  << gibbs_settings.checkConv			   << endl;
    snps << "##Maximum number of Gibbs iterations = "  				  << gibbs_settings.maxIter				   << endl;

    snps << "##Prior hyperparameters for prior distributions of tumour likelihood = " << gibbs_settings.alpha  << endl;
    snps << "##Prior hyperparameters for prior distributions of normal likelihood = " << gibbs_settings.alphaN << endl;
    snps << "##Weight used in prior for conditional distribution in tumour = " 		  << gibbs_settings.weight << endl;

    snps << "##INFO=<ID=NS,Number=1,Type=Integer,Description=\"Number of Samples With Data\">" 		    << endl;
    snps << "##INFO=<ID=DISTR,Number=1,Type=String,Description=\"Allelic composition in all samples\">" << endl;
    snps << "##INFO=<ID=SB,Number=1,Type=Float,Description=\"Strand bias of variant\">" 				<< endl;
    snps << "##FORMAT=<ID=A,Number=1,Type=String,Description=\"Allelic Composition\">"					<< endl;
    snps << "##FORMAT=<ID=GT,Number=1,Type=String,Description=\"Genotype\">"							<< endl;
    snps << "##FORMAT=<ID=GQ,Number=1,Type=Float,Description=\"Genotype Quality\">" 					<< endl;
    snps << "##FORMAT=<ID=SS,Number=1,Type=Integer,Description=\"Somatic Status\">" 					<< endl;
    snps << "##FORMAT=<ID=DP,Number=1,Type=Integer,Description=\"Depth\">" 								<< endl;
    snps << "##FORMAT=<ID=BCOUNT,Number=4,Type=Integer,Description=\"Allele counts\">"					<< endl;

    snps << "#CHROM"  << '\t' << "POS"   << '\t' << "ID" << '\t' << "REF" << '\t'
    	 << "ALT"     << '\t' << "QUAL " << '\t' << " FILTER "   << '\t'  << "INFO" << '\t'
    	 << "FORMAT " << '\t' << "N"     << '\t';

    for(int i = 1; i < program_settings.NumberOfSamples; i++) {
        snps << "T" <<i;
    if(i < program_settings.NumberOfSamples-1)
        snps << '\t';
	}
	snps << endl;
    snps.close();
	return;
}

