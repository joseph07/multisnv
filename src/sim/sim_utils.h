

/*
 *  sim_utils.h
 *  multisnv_proj
 *
 *  Created by Malvina Josephidou on 31/08/2014.
 *  Copyright 2014 University of Cambridge. All rights reserved.
 *
 */
#ifndef ____sim_utils__
#define ____sim_utils__
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <istream>
#include <sstream>
#include <cmath>
#include <stdio.h>
#include <vector>
#include <algorithm>
#include <iterator>
#include <stdlib.h>
#include <map>
#include <utility>
#include <cstring>

#include "main/Settings.h"
#include "main/GibbsSettings.h"
#include "main/SummaryResults.h"
#include "SimulationSettings.h"

int AddNoise (int allele,  double pError);

void write_vcf_sim_header(const FilterSettings& filter_settings,
		const ProgramSettings& program_settings, const GibbsSettings& gibbs_settings,
		const SimulationSettings& sim_settings);


void run_multisnv_sim(const ProgramSettings& program_settings, const GibbsSettings & gibbs_settings,
		const SimulationSettings& sim_settings);

#endif //sim_utils.h
