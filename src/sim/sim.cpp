/*
 *  sim.cpp
 *  multisnv2
 *
 *  Created by Malvina Josephidou on 08/02/2014.
 *  Copyright 2014 University of Cambridge. All rights reserved.
 *
 */

//use instead of main.cpp to simulate data.
#include <iostream>
#include <fstream>
#include <istream>
#include <iomanip>
#include <string>
#include <sstream>
#include <math.h>
#include <stdio.h> 
#include <vector>
#include <algorithm> 
#include <iterator> 
#include <map>
#include <time.h>
#include <unordered_map>
#include <utility>
#include <cstring>
#include <functional>
#include <numeric>
#include <boost/regex.hpp>
#include <boost/program_options.hpp>
#include <errno.h>
#include <unistd.h>
#include <ctype.h>
#include <stdlib.h>

#include "main/rungibbs.h"
#include "main/Settings.h"
#include "SimulationSettings.h"
#include "sim_utils.h"
#include "main/SummaryResults.h"
#include "main/multisnv_input_parser.h"

namespace po = boost::program_options;
using namespace std;

#define BUFFER_LENGTH_LINE 1024*36

int main(int argc, char *argv[])
{
	 /* This MUST come before srand as otherwise
	 random numbers generated with same seed are not the same */

	 clock_t start = clock();

	 FilterSettings filter_settings;
	 ProgramSettings program_settings;
	 GibbsSettings gibbs_settings;
    
	 SimulationSettings sim_settings;
	
	 po::variables_map vm;
	 po::options_description desc = get_multisnv_options_desc (filter_settings,
			program_settings, gibbs_settings);

	 /* Add options specific to the simulation version of multisnv */
	 desc.add_options()
	 ("sim_depth", po::value<int>(& sim_settings.depth)->default_value(30),
	 "Simulated depth")
	 ("sim_maf", po::value<double>(& sim_settings.MAF)->default_value(0.1),
	 "Simulated minor allele freq")
	 ("sim_error", po::value<double>(& sim_settings.pError)->default_value(0.01),
	 "Simulation error")
	 ("sims", po::value<int>(& sim_settings.sims)->default_value(1000),
	 "Number of simulations")
	 ("sim_impurity", po::value<double>(& sim_settings.impurity)->default_value(0),
	 "Percentage impurity in normal sample")
	 ("priv","Simulate a private somatic event")
	 ;
	try {
		po::store(po::parse_command_line(argc, argv, desc), vm);
		po::notify(vm);
		if ((argc==1) or vm.count("help")) {
			cout << desc << "\n";
		return 0;
		}
	}

	catch(po::error& e) {
		cerr << "ERROR: " << e.what() << endl << endl;
		cerr <<  desc     << endl;
		return 1;
	}

	finalise_settings(vm, filter_settings,
			program_settings, gibbs_settings);

	if (vm.count("priv")) {
        sim_settings.isShared = false;
    }

	srand(program_settings.seed);

    write_vcf_sim_header(filter_settings, program_settings, gibbs_settings, sim_settings);

    run_multisnv_sim(program_settings, gibbs_settings, sim_settings);

    double runtime = (double) (clock() - start) / CLOCKS_PER_SEC;

    SummaryResults::getInstance()->print_summary(runtime);
    SummaryResults::destroyInstance(); /* will destroy singleton if it has been instantiated */
	return 0;
	
}



